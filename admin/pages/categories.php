<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); ?>
<!-- Content -->
<div class="content">
    <div class="title"><h5>Kategorije Proizvoda</h5></div>
    <?php $categories1 = Category::find_by_sql("select name_sr, id, active, type_category from category where type_category=1 or type_category=0 or type_category=3 order by active ASC, name_sr ASC"); ?>
    <?php $categories2 = Category::find_by_sql("select name_sr, id, active, type_category from category where type_category=2 or type_category=3 order by active ASC, name_sr ASC"); ?>
	<?php if($session->message()!=''){
		  echo "<br>";
		  if($_SESSION['mType']==2)
            $messageType = 'valid';
		  else
            $messageType = 'invalid';

            echo '<p class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>

          <!-- Dynamic table -->
          <div class="table" style="margin-top:10px">
            <div class="head"><h5 class="iFrames"> <a href="<?=ADMIN?>nova-kategorija">Dodaj kategoriju</a></h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" class="example">
                <thead>
                    <tr>
                        <th>Naziv</th>
                        <th>Active</th>
                        <th> Adminstration </th>
                    </tr>
                </thead>
                <tbody>
                	<?php foreach($categories1 as $cat) {?>
                    <tr class="gradeA" <?=$cat->id?>>
                        <td class="center"> 
                            <a href="<?=ADMIN?>kategorija/<?=$cat->id?>"> <strong><?=$cat->name_sr?><strong> </a>
                        </td>

                        <td class="center"> 
                    	      <input type="checkbox" name="active" <?php if($cat->active == 1){echo 'checked';} ?> />  
                    	</td>

                        <td class="center"> 
                    		<a href="<?=ADMIN?>kategorija/<?=$cat->id?>"> Edit </a> &nbsp;&nbsp;&nbsp; 
                    		<a href="pages/deleteCategory.php?id=<?=$cat->id?>"> Delete </a>                    		
                    	</td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="title" style="margin-top:30px"><h5>Kategorije Galerije</h5></div>
        <div class="table" style="margin-top:10px">
            <div class="head"><h5 class="iFrames"> <a href="<?=ADMIN?>nova-kategorija">Dodaj galeriju</a></h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" class="example">
                <thead>
                    <tr>
                        <th>Naziv</th>
                        <th>Active</th>
                        <th> Adminstration </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($categories2 as $cat) {?>
                    <tr class="gradeA" <?=$cat->id?>>
                        <td class="center"> 
                            <a href="<?=ADMIN?>kategorija/<?=$cat->id?>"> <strong><?=$cat->name_sr?><strong> </a>
                        </td>

                        <td class="center"> 
                              <input type="checkbox" name="active" <?php if($cat->active == 1){echo 'checked';} ?> />  
                        </td>
                        
                        <td class="center"> 
                            <a href="<?=ADMIN?>kategorija/<?=$cat->id?>"> Edit </a> &nbsp;&nbsp;&nbsp; 
                            <a href="pages/deleteCategory.php?id=<?=$cat->id?>"> Delete </a>                            
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        
	</div>
<div class="fix"></div>
</div>




<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Banjica promet d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>