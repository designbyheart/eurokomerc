<?php 
require_once('../../framework/lib/setup.php');

if(isset($_GET['id']) && $_GET['id']!=''){
    $reference = Reference::find_by_id($_GET['id']);
    
if($reference->delete()){
  $session->message('Referenca je izbrisana');
  $_SESSION['mType']= 2;  		
  redirect_to(ADMIN.'reference');
  
}else{
  $session->message('Postoji problem. Referenca nije izbrisana');
  $_SESSION['mType']= 4;  		 
  redirect_to(ADMIN.'reference'); 
}
}

?>