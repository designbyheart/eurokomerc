<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); 

if($_GET['id']==0) $reference = new Reference();
else $reference = Reference::find_by_id($_GET['id']);

?>

<!-- Content -->
<div class="content">
    <div class="title">
        <h5>	
        <?php 
            if($_GET['id']==0){
                ?>Nova referenca<?
            }else{
                ?>Izmena reference<?
            }
          ?>
    	</h5>
    </div>

    <form class="mainForm" action="<?=ADMIN?>saveReference" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset>
        <div class="widget">       
             <ul class="tabs">
                   <li><a href="#tab1">Srpski</a></li>
                   <li><a href="#tab2">Engleski</a></li>
                   <li><a href="#tab3">Ruski</a></li>
                   <li><a href="#tab4">Logo</a></li>
               </ul>

              <div class="tab_container">
                <input type="hidden" name="id" value="<?=$reference->id?>"/>
                <!-- Content of tab1 -->
                <div id="tab1" class="tab_content">
                    <div class="rowElem">
                        <label>Ime Reference:</label>
                        <div class="formRight">
                            <input type = 'text' name="headline_sr" value="<?=$reference->headline_sr?>"     />
                        </div>
                        <div class="fix"></div>
                    </div>

                    <div class="rowElem">
                        <label>Kratak Opis:</label>
                        <div class="formRight">
                            <textarea rows="8" cols="" name="subline_sr" class="wysiwyg"><?=$reference->subline_sr?></textarea>
                        </div>
                        <div class="fix"></div>
                    </div>

                    <div class="rowElem noborder">
                        <label>Url:</label>
                        <div class="formRight">
                            <input type="text" name="url" value="<?=$reference->url?>"/>
                        </div>
                        <div class="fix"></div>
                    </div>

                    <div class="rowElem" style=" width:47%;float:left;">
                        <label>Aktivan:</label>
                        <div class="formRight">
                            <input type="checkbox" name="active" <?php if($reference->active == 1){ echo 'checked'; } ?>/><br>
                        </div>
                        <div class="fix"></div>
                    </div>

                </div>  
                <!-- End - tab1 --> 

                <!-- Content of tab2 -->
                <div id="tab2" class="tab_content">                   
                    <div class="rowElem">
                        <label>Ime Reference:</label>
                        <div class="formRight">
                            <input type = 'text' name="headline_en" value="<?=$reference->headline_en?>"     />
                        </div>
                        <div class="fix"></div>
                    </div>

                    <div class="rowElem">
                        <label>Kratak Opis:</label>
                        <div class="formRight">
                            <textarea rows="8" cols="" name="subline_en" ><?=$reference->subline_en?></textarea>
                        </div>
                        <div class="fix"></div>
                    </div>
                </div>
                <!-- End - tab2 -->
                
                <!-- Content of tab3 -->
                <div id="tab3" class="tab_content">                   
                    
                    <div class="rowElem">
                        <label>Ime Reference:</label>
                        <div class="formRight">
                            <input type = 'text' name="headline_ru" value="<?=$reference->headline_ru?>"     />
                        </div>
                        <div class="fix"></div>
                    </div>

                    <div class="rowElem">
                        <label>Kratak Opis:</label>
                        <div class="formRight">
                            <textarea rows="8" cols="" name="subline_ru" ><?=$reference->subline_ru?></textarea>
                        </div>
                        <div class="fix"></div>
                    </div>
                
                </div>
                <!-- End - tab3 -->  

                <!-- Content of tab4 -->
                <div id="tab4" class="tab_content">
                    
                    <fieldset>

                        <legend>Upload fajla:</legend>
                        <input type="hidden" name="" value="1" class="current" />
                        <input id="file_upload" class="fileInput" type="file" name="file" />
                        <div class="fields"></div><br />
                        <button type="submit" name="gallery" class="uploadImg">Upload</button>
                    </fieldset>

                    <fieldset id="galleryImages">
                        <?php require_once(ADMIN_ROOT.'pages/galleryImages.php'); ?>
                        <div class="imgContent" id="container<?=$reference->id ?>">
                            <?php
                                /*
                                 * <?php if( $_GET['id'] != 0 ) { ?>
                            <a href="<?=ADMIN?>/pages/deleteGallery.php?img_id=<?=$reference->id?>&id=<?=$_GET['id']?>" rel="tooltip" title="Obriši fotografiju" class="delGallery">
                                <img src="<?= SITE_ROOT ?>images/gallery/thumbsM/<?= $reference->file ?>" alt="">
                                <img  class="delImg"  src="<?=ADMIN?>doc/images/icn_alert_error.png" alt="obriši">
                            </a>
                            <?php } ?>
                                 * */
                            ?>
                            <div class="delContent"></div>
                        </div>
                    </fieldset>

                </div>
				<!--  End - tab4  --> 

            </div>	
            <div class="fix"></div>
        </div>
        <!-- End - Tabs -->
    </fieldset>

    <input type="submit" name="submit" value="Submit" class="greyishBtn submitForm" /></form>
</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Eurokomerc d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>
    	