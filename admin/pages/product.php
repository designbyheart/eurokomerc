<?php 
    require_once(ADMIN_ROOT.'doc/inc/side.php'); 
    if($_GET['id']==0)
        $product = new Product();
    else
        $product = Product::find_by_id($_GET['id']);
?>

<!-- Content -->
<div class="content">
    <div class="title">
        <h5>	
    	    <?php if($_GET['id']==0){ ?> Novi proizvod <? } else { ?>Izmena proizvoda<? } ?>
    	</h5>
    </div>
  
  <form class="mainForm" action="<?=ADMIN?>saveProduct" method="post" enctype="multipart/form-data">
    <!-- Tabs -->
  <fieldset>
    <?php if($session->message()!=''){
              if($_SESSION['mType']==2){
                  $messageType = 'valid';
              }else{
                  $messageType = 'invalid';
              }
             echo '<p  class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
          }?>

      <div class="widget">       
        <ul class="tabs">
          <li><a href="#tab1">Srpski</a></li>
          <li><a href="#tab2">Engleski</a></li>
          <li><a href="#tab3">Ruski</a></li>
          <li><a href="#tab5">Galerija</a></li>
          <li><a href="#tab4">Seo</a></li>
        </ul>

        <div class="tab_container">
          <input type="hidden" name="id" value="<?=$product->id?>"/>
          <!-- Content of tab1 -->
          <div id="tab1" class="tab_content">
            
            <div class="rowElem noborder">
              <label>Naziv kategorije:</label>
              <div class="formRight">
                <input type="text" name="name_sr" value="<?=$product->name_sr?>"/>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>Opis:</label>
              <div class="formRight">
                  <div class="widget" style="margin-top: 5px">
                      <textarea class=" descEdit wysiwyg " name='desc_sr'></textarea>

                  </div>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>Active:</label> 
              <div class="formRight">
                <input type="checkbox" name="active" <?php if($product->active == 1){ echo 'checked'; } ?> />
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>Promocija:</label>
              <div class="formRight">
                  <label for="">
                    <input type="checkbox" name="status" <?php if($product->status == 1){ echo 'checked'; } ?> />
                      <span style="position:relative; top:4px; margin-left: 10px;">Aktivaj promociju </span>
                  </label>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem" style="float: left; clear: none; width: 400px">
                <label style="width: 30%;">Kategorija proizvoda:</label>
                <div class="formRight" style="width: 53%;">
                    <select name="category" >
                    <?php
                   //
                        if($product->category!=''){
                            findSubCatsOptionMS($product->category);
                        }else{
                            echo findSubCatsOptionMS();
                        }
                    //
                    ?>
                    </select>

                </div>
                <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>Statistika:</label>
              <div class="formRight">
                Ukupno proizvoda: <b><?php echo Product::count_by_cat($_GET['id']); ?></b> <br>
                Aktivno: <b><?php echo Product::count_by_cat_act($_GET['id']); ?></b>
              </div>
              <div class="fix"></div>
            </div>

          </div>
          <!-- End - tab1 -->

          <!-- Content of tab2 -->
          <div id="tab2" class="tab_content">
            
            <div class="rowElem noborder">
              <label>Naziv kategorije:</label>
              <div class="formRight">
                <input type="text" name="name_en" value="<?=$product->name_en?>"/>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem noborder">
              <label>Opis:</label>
              <div class="formRight">
                <textarea class="descEdit" name='desc_en'></textarea>
              </div>
              <div class="fix"></div>
            </div>
          
          </div>

          <!-- End - tab2 -->

          <!-- Content of tab3 -->
          <div id="tab3" class="tab_content">                   
            
            <div class="rowElem noborder">
              <label>Naziv kategorije:</label>
              <div class="formRight">
                <input type="text" name="name_ru" value="<?=$product->name_ru?>"/>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem noborder">
              <label>Opis:</label>
              <div class="formRight">
                <textarea class="descEdit" name='desc_ru'></textarea>
              </div>
              <div class="fix"></div>
            </div>

          </div>
          <!-- End - tab3 -->
          <div id="tab5" class="tab_content">
              <fieldset>
                  <legend>Upload fotografije za prikaz na strani</legend>
                  <input type="hidden" name="" value="1" class="current" />
                  <input id="file_upload" class="fileInput" type="file" name="image1" />
                  <div class="fields"></div><br />
                  <button name="gallery_page" class="uploadImg">Upload</button>
              </fieldset>
              <fieldset id="galleryImages">
                  <?php
                  $img_type = 'cat';
                  require(ROOT_DIR.'admin/pages/galleryImages.php');
                  ?>
          </div>

          <!-- Content of tab4 -->
          <div id="tab4" class="tab_content">                     
            
            <div class="rowElem noborder">
              <label>Naziv kategorije(srpski):</label>
              <div class="formRight">
                <input type="text" name="seo_title_sr" value="<?=$product->seo_title_sr?>" >
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem noborder">
              <label>Naziv kategorije(engleski):</label>
              <div class="formRight">
                <input type="text" name="seo_title_en" value="<?=$product->seo_title_en?>">
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem noborder">
              <label>Naziv kategorije(ruski):</label>
              <div class="formRight">
                <input type="text" name="seo_title_ru" value="<?=$product->seo_title_ru?>">
              </div>
              <div class="fix"></div>
            </div>            

            <div class="rowElem">
              <label>Opis strane za pretraživače(srpski):</label>
              <div class="formRight">
                <textarea rows="8" cols="" name="seo_desc_sr" ><?=$product->seo_desc_sr?> </textarea>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>Opis strane za pretraživače(engleski):</label>
              <div class="formRight">
                <textarea rows="8" cols="" name="seo_desc_en" ><?=$product->seo_desc_en?></textarea>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>Opis strane za pretraživače(ruski):</label>
              <div class="formRight">
                <textarea rows="8" cols="" name="seo_desc_ru" ><?=$product->seo_desc_ru?></textarea>
              </div>
              <div class="fix"></div>
            </div>            

            <div class="rowElem">
              <label>SEO ključne reči(srpski) :</label>
              <div class="formRight">
                <textarea rows="8" cols="" name="seo_keywords_sr" ><?=$product->seo_keywords_sr?></textarea>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>SEO ključne reči(engleski) :</label>
              <div class="formRight"> 
                <textarea rows="8" cols="" name="seo_keywords_en" ><?=$product->seo_keywords_en?> </textarea>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>SEO ključne reči(ruski) :</label>
              <div class="formRight"> 
                <textarea rows="8" cols="" name="seo_keywords_ru" ><?=$product->seo_keywords_ru?> </textarea>
              </div>
              <div class="fix"></div>
            </div>            

          </div>
          <!-- End - tab4 -->

        </div>  
        <div class="fix"></div>
      </div>
      <!-- End - Tabs -->
      <?php if($_GET['id']==0){ ?> <input type="hidden" name="created" value="<?=time()?>"/> <? } else { ?> <input type="hidden" name="updated" value="<?=time()?>"/> <? } ?>
    </fieldset>
    <input type="submit" name="submit" value="Submit" class="greyishBtn submitForm" />
  </form>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Eurokomerc d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>



