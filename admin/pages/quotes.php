<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); ?>

	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>Citati</h5></div>
        

        
		<?php 
			$quotes = Quote::find_by_sql("SELECT * FROM quotes");
		?>
		
        <?php if($session->message()!=''){
        	  echo "<br>";
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p  class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>
		         
        <!-- Dynamic table -->
        <div class="table">
            <div class="head"><h5 class="iFrames"> <a href="<?=ADMIN?>novi-citat">Dodaj citat</a></h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                    <tr>
                        <th>Naslov</th>
                        <th>Kratak Opis</th>
                        <th> &nbsp;&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                	<?php foreach($quotes as $quote) {?>
                    <tr class="gradeA" >
                        <td class="center"> <a href="<?=ADMIN?>citat/<?=$quote->id?>"> 
                        	<strong><?=$quote->headline_sr?><strong></a>
                        </td>
                    	<td class="center">
                    		<?=$quote->subline_sr ?>
						</td>
                        <td class="center"> 
                    		<a href="<?=ADMIN?>citat/<?=$quote->id?>"> Edit </a> &nbsp;&nbsp;&nbsp; 
                    		<a href="pages/deleteQuotes.php?id=<?=$quote->id?>"> Delete </a>                    		
                    	</td>
                    </tr>
                    <?php 
                		} ?>
                    
                </tbody>
            </table>
        </div>
        
    </div>
<div class="fix"></div>
</div>


<!-- Footer -->
<div id="footer">
	<div class="wrapper">
     	<span>&#169; Copyright <?=date("Y", time())?> Eurokomerc d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


