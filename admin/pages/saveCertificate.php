<?php 
require_once('../../framework/lib/setup.php');

if(isset($_POST['id']) && $_POST['id']!=0 && Reference::find_by_id($_POST['id'])){
    $reference = Reference::find_by_id($_POST['id']);
    $new = false;
}else{
    $reference= new Reference();
    $new = true;
}

if(isset($_POST['active']) && $_POST['active']=="on"){
	$_POST['active']=1; 
}else{
	$_POST['active']=0;
}

    foreach($reference as $key=>$value){
        // echo $_POST[$key];

        if($key!='id' && isset($_POST[$key])){
            $reference->$key = trim($_POST[$key]);
            //echo $reference->$key ."<br>";
        }   	
        
	    if(isset($_POST['date']) && $_POST['date'] != ''){
			$reference->date = date("Y-m-d", strtotime($_POST['date']));
		}else{
			$reference->date = '';
		}
	}

  if(isset($_FILES)){
       foreach($_FILES as  $file){
           if ($file['name']!=''){
               $gal = new Gallery();
               $gal->file = cleanFileName('reference-'.$file['name']);
               $gal->refID = $reference->id;
               $gal->type = 'reference';

               uploadPhoto($file, '', 400, 100, 80, 250, 200, $gal->file);
               if($gal && $gal->save()){
	               $session->message('Slika je uneta');
	               $_SESSION['mType']= 2;
               }else{
               	   $session->message('Postoji problem. Slika nije uneta');
	               $_SESSION['mType']= 4;
               }
           }else{
           		$session->message('Niste odabrali sliku');
	            $_SESSION['mType']= 4;
           }
        }
    }

if(isset($_POST['submit']))	{
	
	if($reference && $reference->save()){
	  $session->message('Referenca je sačuvana');
	
	  $_SESSION['mType']= 2;
	  redirect_to(ADMIN.'reference');
	}else{
	  $session->message('Postoji problem. Referenca nije sačuvana');
	  $_SESSION['mType']= 4;
	  redirect_to(ADMIN.'referenca/'.$reference->id);
	}
}
redirect_to(ADMIN.'referenca/'.$reference->id);
?>