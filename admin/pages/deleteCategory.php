<?php 
require_once('../../framework/lib/setup.php');

if(isset($_GET['id']) && $_GET['id']!=''){
    $category = Category::find_by_id($_GET['id']);
    
if($category->delete()){
  $session->message('Kategorija je izbrisana');
  $_SESSION['mType']= 2;  		
  redirect_to(ADMIN.'kategorije');
  
}else{
  $session->message('Postoji problem. Kategorija nije izbrisana');
  $_SESSION['mType']= 4;  		 
  redirect_to(ADMIN.'kategorije'); 
}
}

?>