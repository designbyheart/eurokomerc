<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); ?>

	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>Poruke</h5></div>
        
        <!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->
        
		<?php 
			$messages = Mail::find_by_sql("SELECT * FROM mail order by status ASC");
		?>
		  <?php if($session->message()!=''){
		  	  echo "<br>";
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>      
        <!-- Dynamic table -->
        <div class="table">
            <div class="head"></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                    <tr>
                        <th>Naslov poruke</th>
                        <th>Pošiljalac</th>
                        <th>Poruka</th>
                        <th>Datum</th>
                        <th>Odgovoreno</th>
                        <th> &nbsp;&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                	<?php foreach($messages as $message) {?>
                    <tr class="gradeA" <?=$message->id?>>
                        <td class="center"> <a href="<?=ADMIN?>poruka/<?=$message->id?>"> 
                        	<strong><?=$message->title?><strong></td>
                        <td align="center">
                             <?=$message->email?>
                        </td>
                    	<td class="center"> 
                    	     <?=trunc($message->messageContent,200)?>
                    	</td>
                    	<td align="center">
                             <?=date("d-m-Y", strtotime($message->date))?>
                        </td>
                        <td align="center">
                        	<input type="checkbox" name="status"
                             <?php if($message->status == 1){
                             		echo "checked";
                             }?>
                             />
                        </td>
                    	<td class="center"> 
                    		<a href="<?=ADMIN?>poruka/<?=$message->id?>"> Edit </a> &nbsp;&nbsp;&nbsp; 
                    		<a href="pages/deleteMessage.php?id=<?=$message->id?>"> Delete </a>                    		
                    	</td>
                    </tr>
                    <?php 
                		} ?>
                    
                </tbody>
            </table>
        </div>
        
	</div>
<div class="fix"></div>
</div>


<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Banjica promet d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


