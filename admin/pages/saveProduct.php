<?php 
require_once('../../framework/lib/setup.php');

if(isset($_POST['id']) && $_POST['id']!=0 && Product::find_by_id($_POST['id'])){
    $product = Product::find_by_id($_POST['id']);
    $new = false;
}else{
    $product= new Product();
    $new = true;
}

if(isset($_POST['active']) && $_POST['active']=="on"){
	$_POST['active']=1; 
}else{
	$_POST['active']=0;
}
if(isset($_POST['promo']) && $_POST['promo']=="on"){
	$_POST['promo']=1; 
}else{
	$_POST['promo']=0;
}

    foreach($product as $key=>$value){
        // echo $_POST[$key];

        if($key!='id' && isset($_POST[$key])){
            $product->$key = trim($_POST[$key]);
            //echo $product->$key ."<br>";
        }   	
	}

//if (isset($_POST['gallery'])){
  if(isset($_FILES)){
       foreach($_FILES as  $file){
           if ($file['name']!=''){
               $gal = new Gallery();
               $gal->file = cleanFileName('product-'.$file['name']);
               $gal->refID = $product->id;
               $gal->type = 'product';

               uploadPhoto($file, '', 600, 100, 80, 250, 200, $gal->file);
               if($gal->save()){
	               $session->message('Slika je uneta');
	               $_SESSION['mType']= 2;
               }else{
               	   $session->message('Postoji problem. Slika nije uneta');
	               $_SESSION['mType']= 4;
               }
           }else{
           		$session->message('Niste odabrali sliku');
	            $_SESSION['mType']= 4;
           }
        }
//    }
}
if(isset($_POST['submit']))	{
	if($product && $product->save()){
	  $product->timestamp = date('Y-m-d h:i:s');	
	  $product->save();
	  $session->message('Proizvod je sačuvan');
	
	  $_SESSION['mType']= 2;
	  redirect_to(ADMIN.'proizvodi');
	}else{
	  $session->message('Postoji problem. Proizvod nije sačuvan');
	  $_SESSION['mType']= 4;
	  redirect_to(ADMIN.'proizvod/'.$product->id);
	}
}
redirect_to(ADMIN.'proizvod/'.$product->id);
?>