<?php 
require_once('../../framework/lib/setup.php');

if(isset($_POST['id']) && $_POST['id']!=0 && Mail::find_by_id($_POST['id'])){
    $message = Mail::find_by_id($_POST['id']);
    $new = false;
}else{
    $message = new Mail();
    $new = true;
}

foreach($message as $key=>$value){
    if($key!='id' && isset($_POST[$key])){
       $message->$key = trim($_POST[$key]);
        //echo $message->$key ."<br>";
    }
}

if($message && $message->save()){
  $session->message('Poruka je sačuvana');
  $_SESSION['mType']= 2;
}else{
  $session->message('Postoji problem. Poruka nije sačuvana');
  $_SESSION['mType']= 4;
}
?>