<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); 

if($_GET['id']==0){
    $message = new Mail();
}else{
    $message = Mail::find_by_id($_GET['id']);
}

if($message->status == 0){ $message->status = 1; $message->save(); }
?>
	<!-- Content -->
    <div class="content">
    	<div class="title"><h5> Detalji poruke </h5>
    	</div>
        
        <!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->

		<form class="mainForm" action="<?=ADMIN?>replayMessage" method="post" enctype="multipart/form-data">

        <fieldset class="originalMessage">
        <?php if($session->message()!=''){
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p  class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>
		  
        	<div class="widget" style="border-top: 1px solid #D5D5D5; margin-top: 10px;">       
                                 			
              		<input type="hidden" name="id" value="<?=$message->id?>"/>
              		
                    	<div class="rowElem noborder">
                    		<label>Naslov poruke:</label>
                    		<div class="formRight">
                    			<input type="text" name="message" value="<?=$message->title?> "/>
                    		</div>
                    		<div class="fix"></div>
                    	</div> 
                    	<div class="rowElem noborder">
                    		<label>E-mail:</label>
                    		<div class="formRight">
                    			<input type="text" name="email" value="<?=$message->email?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div> 
                    	<div class="rowElem noborder">
                    		<label>Name:</label>
                    		<div class="formRight">
                    			<input type="text" name="name" value="<?=$message->name?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>   
                    	<div class="rowElem">
		                    <label>Tekst poruke:</label>
		                    <div class="formRight">
		                    	<textarea rows="5" cols="" name="messageContent" class="wysiwyg"><?=$message->messageContent?></textarea></div>
		                    <div class="fix"></div>
		                 </div>                  
	                                                                                                                           
               <div class="fix"></div>		 
         </div>
		 </fieldset>
		 <input type="submit" name="submit" value="Odgovori" class="greyishBtn submitForm" id="reply" style="float: left;  margin: 0 23px;"/>
		 		 
		 <br><br>
		 <fieldset class="replyForm"> <legend> Odgovor </legend>
		 	<div class="widget" style="border-top: 1px solid #D5D5D5; margin-top: 10px;">
		 		<div class="rowElem noborder">
                   	<label>Naslov:</label>
                    <div class="formRight">
                    	<input type="text" name="reftitle" value="<?php echo "RE:[".$message->title."]"?>"/>
                    </div>
                    <div class="fix"></div>
                 </div>  
                 <div class="rowElem noborder">
                   	<label>Primalac:</label>
                    <div class="formRight">
                    	<input type="text" name="to" value="<?=$message->email?>" />
                    </div>
                    <div class="fix"></div>
                 </div> 
                 <div class="rowElem noborder">
                   	<label>Pošiljalac:</label>
                    <div class="formRight">
                    	<?php $admin = Administrator::find_by_id($_SESSION['user_id']);?>
                    	<input type="text" name="from" value="<?=$admin->e_mail?>" />
                    </div>
                    <div class="fix"></div>
                 </div> 
                 <div class="rowElem">
                    <label>Tekst poruke:</label>
                    <div class="formRight">
                    	<textarea rows="5" cols="" name="refMessageContent" ></textarea></div>
                    <div class="fix"></div>
                 </div>
		 	
		 	</div>
            <input type="submit" name="submit" value="Pošalji" class="greyishBtn submitForm" style="float: left;  margin: 10px 23px 0 0;"/>
		 </fieldset>

		 </form>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Banjica promet d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


