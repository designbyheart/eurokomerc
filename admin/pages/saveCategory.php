<?php
require_once('../../framework/lib/setup.php');

if(isset($_POST['id']) && $_POST['id']!=0 && Category::find_by_id($_POST['id'])){
    $category = Category::find_by_id($_POST['id']);
    $new = false;
}else{
    $category= new Category();
    $new = true;
}

if( isset( $_POST['type1'] ) && isset( $_POST['type2']) ) $_POST['type_category'] = 3;
elseif( isset( $_POST['type1'] ) && !isset( $_POST['type2']) ) $_POST['type_category'] = 1;
elseif( !isset( $_POST['type1'] ) && isset( $_POST['type2']) ) $_POST['type_category'] = 2;
else  $_POST['type'] = 1;

if(isset($_POST['active']) && $_POST['active']=="on"){
	$_POST['active']=1; 
}else{
	$_POST['active']=0;
}
    foreach($category as $key=>$value){
        // echo $_POST[$key];

        if($key!='id' && isset($_POST[$key])){
            $category->$key = trim($_POST[$key]);
            //echo $category->$key ."<br>";
        }   	
	}



//var_dump($_FILES);
if(isset($_POST['gallery_menu']))	{
  if(isset($_FILES['image0'])){
      //image for menu
  		$file = $_FILES['image0'];
           if ($file['name']!=''){
               $gal = new Gallery();
               $gal->file = cleanFileName('cat-menu-'.$file['name']);
               $gal->refID = $category->id;
               $gal->type = 'cat-menu';

               uploadPhoto($file, '', 250, 100, 80, 250, 200, $gal->file);
               if($gal->save()){
	               $session->message('Slika je uneta');
	               $_SESSION['mType']= 2;
               }else{
               	   $session->message('Postoji problem. Slika nije uneta1');
	               $_SESSION['mType']= 4;
               }
           }else{
           		$session->message('Niste odabrali sliku');
	            $_SESSION['mType']= 4;
           }
  }        
}

if(isset($_POST['gallery_page']))	{
	if(isset($_FILES['image1'])){
        //image for category page
  	       $file = $_FILES['image1'];
           if ($file['name']!=''){
               $gal = new Gallery();
               $gal->file = cleanFileName('cat-'.$file['name']);
               $gal->refID = $category->id;
               $gal->type = 'cat';

               uploadPhoto($file, '', 370, 100, 80, 250, 200, $gal->file);
               if($gal->save()){
	               $session->message('Slika je uneta');
	               $_SESSION['mType']= 2;
               }else{
               	   $session->message('Postoji problem. Slika nije uneta2');
	               $_SESSION['mType']= 4;
               }
           }else{
           		$session->message('Niste odabrali sliku');
	            $_SESSION['mType']= 4;
           }
    }
}
  
if(isset($_POST['submit']))	{
	if($category && $category->save()){	  	
	  $session->message('Kategorija je sačuvana');
	
	  $_SESSION['mType']= 2;
	  redirect_to(ADMIN.'kategorije');
	}else{
	  $session->message('Postoji problem. Kategorija nije sačuvana3');
	  $_SESSION['mType']= 4;
	  redirect_to(ADMIN.'kategorija/'.$category->id);
	}
}
redirect_to(ADMIN.'kategorija/'.$category->id);

?>