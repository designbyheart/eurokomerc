<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 5/1/12
 * Time: 9:49 AM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');

if(isset($_POST['saveQuote'])){
    //$q;
    //check is q already exists
    if(isset($_POST['id']) && $_POST['id']!=''){
        $q = Quote::find_by_id($_POST['id']);
    }
    if(!isset($q)){
        $q = new Quote();
    }
    if($q){
        foreach($q as $key=>$value){
            if(isset($_POST[$key]) && $key !='id'){
                $q->$key = $_POST[$key];
            }
        }
    }
    if($q && $q->save()){
        $session->message("Tekst naslovne strane je sacuvan");
        redirect_to(ADMIN.'dashboard');
    }
    else{
        redirect_to(ADMIN.'dashboard');
    }
}else
{
    echo "Ne ulazi";
}
