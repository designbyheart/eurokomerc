<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); ?>

	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>Proizvodi</h5></div>
        

        
		<?php 
			 $products = Product::find_by_sql("SELECT *, (select name_sr from category where category.id = product.category) as category FROM product order by category ASC, active ASC, name_sr ASC");

		?>
		
          <?php if($session->message()!=''){
		  	  echo "<br>";
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?> 
               
        <!-- Dynamic table -->
        <div class="table">
            <div class="head"><h5 class="iFrames"> <a href="<?=ADMIN?>novi-proizvod">Dodaj proizvod</a></h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                    <tr>
                        <th>Naziv</th>
                        <th>Kategorija</th>
                        <th>Opis</th>
                        <th>Aktivan</th>
                        <th>Status</th>
                        <th> &nbsp;&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                	<?php foreach($products as $product) {?>
                    <tr class="gradeA" <?=$product->id?>>
                        <td class="center"> <a href="<?=ADMIN?>proizvod/<?=$product->id?>"> 
                        	<strong><?=$product->name_sr?><strong></a></td>
                    	<td class="center"> <?=$product->category ?></td>
                    	<td class="center"> <?=$product->desc_sr ?></td>
                    	
                    	<td class="center"> 
                    	      <input type="checkbox" name="active" 
					         	  <?php if($product->active == 1){
					               		 echo 'checked';
					              }
					              ?>
				         	  />	
                    	</td>
                    	<td class="center"> 
                    	      <input type="checkbox" name="status" 
					         	  <?php if($product->status == 1){
					               		 echo 'checked';
					              }
					              ?>
				         	  />	
                    	</td>
                    	<td class="center"> 
                    		<a href="<?=ADMIN?>proizvod/<?=$product->id?>"> Edit </a> &nbsp;&nbsp;&nbsp; 
                    		<a href="pages/deleteProduct.php?id=<?=$product->id?>"> Delete </a>                    		
                    	</td>
                    </tr>
                    <?php 
                		} ?>
                    
                </tbody>
            </table>
        </div>
        
    </div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Eurokomerc d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>

