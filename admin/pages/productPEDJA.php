<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); 

if($_GET['id']==0){
    $product = new Product();
}else{
    $product = Product::find_by_id($_GET['id']);
}
?>
	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>	
    	    <?php 
            if($_GET['id']==0){
                ?>Novi proizvod<?
            }else{
                ?>Izmena proizvoda<?
            }
           
          ?>
    	</h5></div>
        
        <!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->
        
		<form class="mainForm" action="<?=ADMIN?>saveProduct" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset>
          <?php if($session->message()!=''){
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?> 
        <div class="widget">       
             <ul class="tabs">
                   <li><a href="#tab1">Srpski</a></li>
                   <li><a href="#tab2">Engleski</a></li>
                   <li><a href="#tab3">Galerija</a></li>
                   <li><a href="#tab4">Seo</a></li>
              </ul>
                    
              <div class="tab_container">
              			
              		<input type="hidden" name="id" value="<?=$product->id?>"/>
              		
                    <!-- Content of tab1 -->
                    <div id="tab1" class="tab_content">
                    	<div class="rowElem noborder">
                    		<label>Naziv proizvoda:</label>
                    		<div class="formRight">
                    			<input type="text" name="name_sr" value="<?=$product->name_sr?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>   
                    	
	                    <div class="rowElem" style="float: left; clear: none; width: 400px">
	                        <label style="width: 30%;">Kategorija proizvoda:</label>
	                        <div class="formRight" style="width: 53%;">
                                <select name="category" >
                                <?php
                                if($product->category!=''){
                                    $category = Category::find_by_id($product->category);
                                    findSubCatsOption($category);
                                }else{
                                    findSubCatsOption();
                                }
                                ?>
	                        </select>
	                        </div>
	                        <div class="fix"></div>
	                    </div>
	                    
	                    <div class="rowElem " style="float: left; clear: none; width: 300px">
                    		<label style="width: 25%;">Serijski broj:</label>
                    		<div class="formRight" style="width: 60%;">
                    			<input type="text" name="plD" value="<?=$product->plD?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div> 
	                    
	                    <div class="rowElem">
	                        <label>Proizvodjač:</label>
	                        <div class="formRight">
	                        <select name="manufacturer" >
	                            <option value="0">Izaberi proizvođača</option>
					            <?php             	
						           	$manufacturers = Manufacturer::find_all();	           	  
						          	foreach ($manufacturers as $manufacturer) : ?>
					          		<option value="<?=$manufacturer->id?>" 
					          		<?php if($product->id >0){
					          			  // manufacturer_p je proizvodjac datog proizvoda
					          			  $manufacturer_p = Manufacturer::find_by_id($product->manufacturer);
							          	  if(isset($manufacturer_p->id) && $manufacturer->id == $manufacturer_p->id){ echo 'selected';}
							          	  }?>
					          		> <?=$manufacturer->name_sr?></option>
					          	<?php endforeach; ?>
	                        </select>
	                        </div>
	                        <div class="fix"></div>
                    	</div>          
                    	                       
                    	
                    	<div class="rowElem">
	                        <label>Active:</label> 
	                        <div class="formRight">
	                               <input type="checkbox" name="active" 
						         	  <?php if($product->active == 1){
						               		 echo 'checked';
						              }
					              ?>
				         	  />
	                        </div>
	                        <div class="fix"></div>
                    	</div>
                    	
                    	<div class="rowElem">
	                        <label>Na promociji:</label> 
	                        <div class="formRight">
	                               <input type="checkbox" name="promo" 
						         	  <?php if($product->promo == 1){
						               		 echo 'checked';
						              }
					              ?>
				         	  />
	                        </div>
	                        <div class="fix"></div>
                    	</div>
                    
                  	    <div class="rowElem">
	                        <label>Status:</label> 
	                        <div class="formRight">
	                        	<input type="radio" name="status" value="0" <?php if($product->status == 0) {echo 'checked';}?>/><label>Na stanju</label>
	                            <input type="radio" name="status" value="1" <?php if($product->status == 1) {echo 'checked';}?>/><label>Nema ga na stanju</label>
	                            <input type="radio" name="status" value="2" <?php if($product->status == 2) {echo 'checked';}?>/><label>Nije u prodaji</label>
	                        </div>
	                        <div class="fix"></div>
                   	    </div>            
                    	
                    	<div class="rowElem">
                    		<label>Opis proizvoda:</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="desc_sr" ><?=trim($product->desc_sr)?></textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	
                    	<div class="rowElem">
                    		<label>Veličina proizvoda (Dimenzije):</label>
                    		<div class="formRight">
                    			<textarea rows="2" cols="" name="size_sr" ><?=$product->size_sr?></textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	
                    	<div class="rowElem">
                    		<label>Snaga proizvoda(KW):</label>
                    		<div class="formRight">
                    			<textarea rows="2" cols="" name="power_sr" ><?=$product->power_sr?></textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	
                    	<div class="rowElem">
                    		<label>Vrsta napajanja:</label>
                    		<div class="formRight">
                    			<textarea rows="2" cols="" name="power_type_sr" ><?=$product->power_type_sr?></textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	
                    	<div class="rowElem">
                    		<label>Karakteristike proizvoda:</label>
                    		<div class="formRight">
                    			<textarea rows="2" cols="" name="characteristics_sr" ><?=$product->characteristics_sr?></textarea></div>
                    		<div class="fix"></div>
                    	</div>
                   	
                        
                    </div>  
                    <!-- End - tab1 --> 
                                                                              
                    <!-- Content of tab2 -->
                    <div id="tab2" class="tab_content">                   
                   		<div class="rowElem noborder">
                    		<label>Naziv proizvoda:</label>
                    		<div class="formRight">
                    			<input type="text" name="name_en" value="<?=$product->name_en?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div> 
                    	<div class="rowElem">
                    		<label>Opis proizvoda:</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="desc_en" ><?=$product->desc_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	
                    	<div class="rowElem">
                    		<label>Veličina proizvoda:</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="size_en" ><?=$product->size_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	
                    	<div class="rowElem">
                    		<label>Snaga proizvoda:</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="power_en" ><?=$product->power_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	
                    	<div class="rowElem">
                    		<label>Snaga tipa proizvoda:</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="power_type_en" ><?=$product->power_type_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	
                    	<div class="rowElem">
                    		<label>Karakteristike proizvoda:</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="characteristics_en" ><?=$product->characteristics_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    </div>
                    <!-- End - tab2 --> 
                    
                    <!-- Content of tab3 -->
                    <div id="tab3" class="tab_content">
			          <fieldset>
			            <legend>Upload fotografija <a href="#" id="addMoreFields">+</a></legend>
			            <input type="hidden" name="" value="1" class="current" />
			             <input id="file_upload" class="fileInput" type="file" name="image0" />
			             <div class="fields"></div><br />
			             <button type="submit" name="gallery" class="uploadImg">Upload</button>
			           </fieldset>
			           <fieldset id="galleryImages">
			              <?php
			              //$_GET['page'] = $event->event_type; 
			                require_once(ROOT_DIR.'admin/pages/galleryList.php');
			              ?>
			           </fieldset>
                    </div>
                    <!-- End - tab3 --> 
                    
                    <!-- Content of tab4 -->
                    <div id="tab4" class="tab_content">                     
                    	<div class="rowElem noborder">
                    		<label>Naziv proizvoda(srpski):</label>
                    		<div class="formRight">
                    			<input type="text" name="seo_title_sr" value="<?=$product->seo_title_sr?>">
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem noborder">
                    		<label>Naziv proizvoda(engleski):</label>
                    		<div class="formRight">
                    			<input type="text" name="seo_title_en" value="<?=$product->seo_title_en?>">
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                        <div class="rowElem">
                    		<label>Opis strane za pretraživače(srpski):</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_desc_sr" ><?=$product->seo_desc_sr?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>Opis strane za pretraživače(engleski):</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_desc_en" ><?=$product->seo_desc_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>SEO ključne reči(srpski) :</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_keywords_sr" ><?=$product->seo_keywords_sr?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>SEO ključne reči(engleski) :</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_keywords_en" ><?=$product->seo_keywords_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    
                    </div>
                    <!-- End - tab4 -->
                    
               </div>	
               <div class="fix"></div>		 
         </div>
         <!-- End - Tabs -->
		 </fieldset>
		 <input type="submit" name="submit" value="Submit" class="greyishBtn submitForm" />
	 </form>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Eurokomerc d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>



