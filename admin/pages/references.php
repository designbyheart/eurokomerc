<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); ?>

	
    <div class="content">
    	<div class="title"><h5>Reference</h5></div>
        

        
		<?php 
			$references = Reference::find_by_sql("SELECT * FROM reference order by active ASC");
		?>
		
        <?php if($session->message()!=''){
        	  echo "<br>";
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p  class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>
		         
        <!-- Dynamic table -->
        <div class="table">
            <div class="head"><h5 class="iFrames"> <a href="<?=ADMIN?>nova-referenca">Dodaj referencu</a></h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                    <tr>
                        <th>Ime Reference</th>
                        <th>Kratak Opis</th>
                        <th>Active</th>                    
                        <th>Url</th>       
                        <th> &nbsp;&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                	<?php foreach($references as $reference) {?>
                    <tr class="gradeA" >
                        <td class="center"> <a href="<?=ADMIN?>referenca/<?=$reference->id?>"> 
                        	<strong><?=$reference->headline_sr?><strong></a></td>
                    	<td class="center">
                    		<?=$reference->subline_sr ?>
						</td>
                    	
                    	<td class="center"> 
                    	      <input type="checkbox" name="active" 
					         	  <?php if($reference->active == 1){
					               		 echo 'checked';
					              }
					              ?>
				         	  />	
                    	</td>
                        <td class="center"> 
                        	<?=$reference->url ?>
                         </td>

                    	<td class="center"> 
                    		<a href="<?=ADMIN?>referenca/<?=$reference->id?>"> Edit </a> &nbsp;&nbsp;&nbsp; 
                    		<a href="pages/deleteReference.php?id=<?=$reference->id?>"> Delete </a>                    		
                    	</td>
                    </tr>
                    <?php 
                		} ?>
                    
                </tbody>
            </table>
        </div>
        
    </div>
<div class="fix"></div>
</div>


<!-- Footer -->
<div id="footer">
	<div class="wrapper">
     	<span>&#169; Copyright <?=date("Y", time())?> Eurokomerc d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


