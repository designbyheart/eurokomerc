<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); 

if($_GET['id']==0) $quote = new Quote();
else $quote = Quote::find_by_id($_GET['id']);

?>

<!-- Content -->
<div class="content">
    <div class="title">
        <h5>	
        <?php 
            if($_GET['id']==0){
                ?>Novi Citat<?
            }else{
                ?>Izmena Citata-a<?
            }
          ?>
    	</h5>
    </div>

    <form class="mainForm" action="<?=ADMIN?>saveQuote" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset>
        <div class="widget">       
             <ul class="tabs">
                   <li><a href="#tab1">Srpski</a></li>
                   <li><a href="#tab2">Engleski</a></li>
                   <li><a href="#tab3">Ruski</a></li>
            </ul>

              <div class="tab_container">
                <input type="hidden" name="id" value="<?=$quote->id?>"/>
                <!-- Content of tab1 -->
                <div id="tab1" class="tab_content">
                    <div class="rowElem">
                        <label>Naslov:</label>
                        <div class="formRight">
                            <textarea rows="8" cols="" name="headline_sr" ><?=$quote->headline_sr?> </textarea>
                        </div>
                        <div class="fix"></div>
                    </div>

                    <div class="rowElem">
                        <label>Kratak Opis:</label>
                        <div class="formRight">
                            <textarea rows="8" cols="" name="subline_sr" ><?=$quote->subline_sr?></textarea>
                        </div>
                        <div class="fix"></div>
                    </div>

                </div>  
                <!-- End - tab1 --> 

                <!-- Content of tab2 -->
                <div id="tab2" class="tab_content">                   
                    <div class="rowElem">
                        <label>Naslov:</label>
                        <div class="formRight">
                            <textarea rows="8" cols="" name="headline_en" ><?=$quote->headline_en?></textarea>
                        </div>
                        <div class="fix"></div>
                    </div>

                    <div class="rowElem">
                        <label>Kratak Opis:</label>
                        <div class="formRight">
                            <textarea rows="8" cols="" name="subline_en" ><?=$quote->subline_en?></textarea>
                        </div>
                        <div class="fix"></div>
                    </div>
                </div>
                <!-- End - tab2 -->
                
                <!-- Content of tab3 -->
                <div id="tab3" class="tab_content">                   
                    
                    <div class="rowElem">
                        <label>Naslov:</label>
                        <div class="formRight">
                            <textarea rows="8" cols="" name="headline_ru" ><?=$quote->headline_ru?></textarea>
                        </div>
                        <div class="fix"></div>
                    </div>

                    <div class="rowElem">
                        <label>Kratak Opis:</label>
                        <div class="formRight">
                            <textarea rows="8" cols="" name="subline_ru" ><?=$quote->subline_ru?></textarea>
                        </div>
                        <div class="fix"></div>
                    </div>
                
                </div>
                <!-- End - tab3 -->  

                </div>	
            <div class="fix"></div>
        </div>
        <!-- End - Tabs -->
    </fieldset>

    <input type="submit" name="submit" value="Submit" class="greyishBtn submitForm" /></form>
</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Eurokomerc d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>
    	