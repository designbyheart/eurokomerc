<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 5/5/12
 * Time: 10:23 PM
 * To change this template use File | Settings | File Templates.
 */

require_once(ADMIN_ROOT.'doc/inc/side.php'); ?>

	<!-- Content -->
    <div class="content">
        <div class="title"><h5>Baneri - naslovna strana</h5></div>

        <!-- Statistics -->
        <!--        <div class="stats">-->
        <!--        	<ul>-->
        <!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
        <!--                -->
        <!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
        <!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
        <!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
        <!--            </ul>-->
        <!--            <div class="fix"></div>-->
        <!--        </div>-->

        <?php
        $pages = GeneralPage::find_all();
        ?>
        <?php if($session->message()!=''){
        echo "<br>";
        if($_SESSION['mType']==2){
            $messageType = 'valid';
        }else{
            $messageType = 'invalid';
        }
        echo '<p class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>';
    }?>
        <!-- Dynamic table -->
        <div class="table">
            <div class="head"><h5 class="iFrames"> <a href="<?=ADMIN?>nova-strana">Dodaj opštu stranu</a></h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                <tr>
                    <th>Naziv</th>
                    <th>Active</th>
                    <th> &nbsp;&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($pages as $page) {?>
                <tr class="gradeA" <?=$page->id?>>
                    <td class="center">
                        <a href="<?=ADMIN?>opsta/<?=$page->pg_name?>">
                            <strong><?=$page->name_sr?><strong></a>
                    </td>
                    <td class="center">
                        <input type="checkbox" name="active"
                            <?php if($page->active == 1){
                            echo 'checked';
                        }
                            ?>
                            />
                    </td>
                    <td class="center">
                        <a href="<?=ADMIN?>opsta/<?=$page->pg_name?>"> Edit </a> &nbsp;&nbsp;&nbsp;
                        <a href="pages/deletePage.php?id=<?=$page->id?>"> Delete </a>
                    </td>
                </tr>
                    <?php
                } ?>

                </tbody>
            </table>
        </div>

    </div>
<div class="fix"></div>
</div>


<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&#169; Copyright <?=date("Y", time())?> Banjica promet d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


