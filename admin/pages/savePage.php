<?php 
require_once('../../framework/lib/setup.php');

if(isset($_POST['id']) && $_POST['id']!=0 && GeneralPage::find_by_id($_POST['id'])){
    $page = GeneralPage::find_by_id($_POST['id']);
    $new = false;
}else{
    $page = new GeneralPage();
    $new = true;
}

if(isset($_POST['active']) && $_POST['active']=="on"){
	$_POST['active']=1; 
}else{
	$_POST['active']=0;
}

    foreach($page as $key=>$value){
        // echo $_POST[$key];
        if($key!='id' && isset($_POST[$key])){
            $page->$key = trim($_POST[$key]);
        }   	
	}
$page->desc_en = clearFont($page->desc_en);
$page->desc_ru = clearFont($page->desc_ru);
$page->desc_sr = clearFont($page->desc_sr);
	
    if(isset($_POST['name_sr'])){
    	//replace one or more spaces with one
		$name = preg_replace("/[[:blank:]]+/"," ",$_POST['name_sr']);
		//replace spaces with '-', and all letters with small letters
		//trim() - remove space from the beginning and end of the string
        $page->pg_name = $_POST['pg_name'];//strtolower(str_replace(" ", "-", trim($name)));
    }
        
if (isset($_POST['gallery'])){
  if(isset($_FILES)){
       foreach($_FILES as  $file){
           if ($file['name']!=''){
               $gal = new Gallery();
               $gal->file = cleanFileName('general_page-'.$file['name']);
               $gal->refID = $page->id;
               $gal->type = 'general_page';

               uploadPhoto($file, '', 400, 100, 80, 250, 200, $gal->file);
               if($gal->save()){
	               $session->message('Slika je uneta');
	               $_SESSION['mType']= 2;
               }else{
               	   $session->message('Postoji problem. Slika nije uneta');
	               $_SESSION['mType']= 4;
               }
           }else{
           		$session->message('Niste odabrali sliku');
	            $_SESSION['mType']= 4;
           }
        }
    }
}
if($page && $page->save()){
  $session->message('Strana je sačuvana');
  $_SESSION['mType']= 2;

  redirect_to(ADMIN.'opste_strane');
}else{
  $session->message('Postoji problem. Strana nije sačuvana');
  $_SESSION['mType']= 4;
  redirect_to(ADMIN.'opsta/'.$page->pg_name);
}
?>
