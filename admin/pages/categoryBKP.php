<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); 

if($_GET['id']==0){
    $category = new Category();
}else{
    $category = Category::find_by_id($_GET['id']);
}
?>
	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>	
    	    <?php 
            if($_GET['id']==0){
                ?>Nova kategorija<?
            }else{
                ?>Izmena kategorije<?
            }
          ?>
    	</h5></div>
        
        <!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->
        
		<form class="mainForm" action="<?=ADMIN?>saveCategory" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset>
        <?php if($session->message()!=''){
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p  class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>
        <div class="widget">       
             <ul class="tabs">
                   <li><a href="#tab1">Srpski</a></li>
                   <li><a href="#tab2">Engleski</a></li>
                   <li><a href="#tab3">Galerija</a></li>
                   <li><a href="#tab4">Seo</a></li>
              </ul>
                    
              <div class="tab_container">
              			
              		<input type="hidden" name="id" value="<?=$category->id?>"/>
              		
                    <!-- Content of tab1 -->
                    <div id="tab1" class="tab_content">
                    	<div class="rowElem noborder">
                    		<label>Naziv kategorije:</label>
                    		<div class="formRight">
                    			<input type="text" name="name_sr" value="<?=$category->name_sr?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>                     
	                           
  						<div class="rowElem">
	                        <label>Nadređena kategorija:</label>
	                        <div class="formRight">
	                        <select name="parent_cat" >
	                            <option value="0">Izaberi kategoriju</option>
					            <?php             	
						           	$categories = Category::find_by_sql("SELECT id, name_sr, parent_cat FROM category where parent_cat = 0 and id != {$_GET['id']}");	           	  
						          	foreach ($categories as $cat) : ?>
					          		<option value="<?=$cat->id?>" 
					          		<?php if($category->id >0){
							          	  if(isset($category->id) && $cat->id == $category->parent_cat){ echo 'selected';}
							          	  }?>
					          		> <?=$cat->name_sr?></option>
					          	<?php endforeach; ?>
	                        </select>
	                        </div>
	                        <div class="fix"></div>
	                    </div>   
	                    	
                    	<div class="rowElem">
	                        <label>Active:</label> 
	                        <div class="formRight">
	                               <input type="checkbox" name="active" 
						         	  <?php if($category->active == 1){
						               		 echo 'checked';
						              }
					              ?>
				         	  />
	                        </div>
	                        <div class="fix"></div>
                    	</div>
                                    	                            
                    </div>  
                    <!-- End - tab1 --> 
                                         
                                         
                    <!-- Content of tab2 -->
                    <div id="tab2" class="tab_content">                   
                   		<div class="rowElem noborder">
                    		<label>Naziv kategorije:</label>
                    		<div class="formRight">
                    			<input type="text" name="name_en" value="<?=$category->name_en?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>                    	
                    </div>
                    <!-- End - tab2 --> 
                    
                    <!-- Content of tab3 -->
                    <div id="tab3" class="tab_content">
			          <fieldset>
			            <legend>Upload fotografije za meni</legend>
			            <input type="hidden" name="" value="1" class="current" />
			              <input id="file_upload" class="fileInput" type="file" name="image0" />
			              <div class="fields"></div><br />
			              <button name="gallery_menu" class="uploadImg">Upload</button>
			           </fieldset>
			           <fieldset id="galleryImages">
			              <?php 
			                 $img_type = 'cat-menu';
			                 require(ROOT_DIR."admin/pages/galleryList.php");
			              ?>
			           </fieldset>
			           
			           	<fieldset>
			            <legend>Upload fotografije za prikaz na strani</legend>
			            <input type="hidden" name="" value="1" class="current" />
			              <input id="file_upload" class="fileInput" type="file" name="image1" />
			              <div class="fields"></div><br />
			              <button name="gallery_page" class="uploadImg">Upload</button>
			           </fieldset>
			           <fieldset id="galleryImages">
			              <?php 
			                $img_type = 'cat';
			                require(ROOT_DIR.'admin/pages/galleryList.php');
			              ?>
			           </fieldset>
                    </div>
                    <!-- End - tab3 --> 
                    
                    <!-- Content of tab4 -->
                    <div id="tab4" class="tab_content">                     
                    	<div class="rowElem noborder">
                    		<label>Naziv kategorije(srpski):</label>
                    		<div class="formRight">
                    			<input type="text" name="seo_title_sr" value="<?=$category->seo_title_sr?>" >
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem noborder">
                    		<label>Naziv kategorije(engleski):</label>
                    		<div class="formRight">
                    			<input type="text" name="seo_title_en" value="<?=$category->seo_title_en?>">
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                        <div class="rowElem">
                    		<label>Opis strane za pretraživače(srpski):</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_desc_sr" ><?=$category->seo_desc_sr?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>Opis strane za pretraživače(engleski):</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_desc_en" ><?=$category->seo_desc_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>SEO ključne reči(srpski) :</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_keywords_sr" ><?=$category->seo_keywords_sr?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>SEO ključne reči(engleski) :</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_keywords_en" ><?=$category->seo_keywords_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    
                    </div>
                    <!-- End - tab4 -->
                    
               </div>	
               <div class="fix"></div>		 
         </div>
         <!-- End - Tabs -->
		 </fieldset>
		 <input type="submit" name="submit" value="Submit" class="greyishBtn submitForm" />
		 </form>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Eurokomerc d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


