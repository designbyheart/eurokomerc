<?php 
require_once('../../framework/lib/setup.php');

if(isset($_POST['id']) && $_POST['id']!=0 && News::find_by_id($_POST['id'])){
    $news = News::find_by_id($_POST['id']);
    $new = false;
}else{
    $news = new News();
    $new = true;
}
if(isset($_POST['active'])){
	$_POST['active']=1; 
}else{
	$_POST['active']=0;
}
//$date = explode("-", $_POST['date']);
//$_POST['date'] = mktime(0, 0, 0, $date[1], $date[0], $date[2]);

    foreach($news as $key=>$value){
        // echo $_POST[$key];
        if($key!='id' && isset($_POST[$key])){
            $news->$key = trim($_POST[$key]);
          //  echo $news->$key ."<br>";
        }   	
	}
$news->content_en = clearFont($news->content_en);
$news->content_sr = clearFont($news->content_sr);
$news->content_ru = clearFont($news->content_ru);
$news->date = date("Y-m-d", strtotime($_POST['date']));
//if (isset($_POST['gallery'])){
  if(isset($_FILES)){
       foreach($_FILES as  $file){
           if ($file['name']!=''){
               $gal = new Gallery();
               $gal->file = cleanFileName('aktuelnost-'.$file['name']);
               $gal->refID = $news->id;
               $gal->type = 'aktuelnost';

               uploadPhoto($file, '', 400, 100, 80, 250, 200, $gal->file);
               if($gal->save()){
	               $session->message('Slika je uneta');
	               $_SESSION['mType']= 2;
               }else{
               	   $session->message('Postoji problem. Slika nije uneta');
	               $_SESSION['mType']= 4;
               }
           }else{
           		$session->message('Niste odabrali sliku');
	            $_SESSION['mType']= 4;
           }
        }
}
if(isset($_POST['submit']))	{
	if($news && $news->save()){	
	  $session->message('Aktuelnost je sačuvana');
	
	  $_SESSION['mType']= 2;
	  redirect_to(ADMIN.'aktuelnosti');
	}else{
	  $session->message('Postoji problem. Aktuelnost nije sačuvana');
	  $_SESSION['mType']= 4;
	  redirect_to(ADMIN.'aktuelnost/'.$news->id);
	}
}
redirect_to(ADMIN.'aktuelnost/'.$news->id);
