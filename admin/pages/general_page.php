<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); ?>
<?php 
if(isset($_GET['pg']) && $_GET['pg'] != ''){
$pg = $_GET['pg'];
$page = GeneralPage::find_by_sql("SELECT * FROM general_page WHERE pg_name = '{$pg}'");
$page = array_shift($page);
}else{
	$page = new GeneralPage();
}
//var_dump($page);
?>
<!-- Content -->
    <div class="content">
    	<div class="title"><h5>
    	    <?php 
            if(isset($_GET['pg']) && $_GET['pg'] !=''){
                ?>Izmena strane - <?
            }else{
                ?>Nova strana <?
            }
          ?>
    	 <?=$page->name_sr?>	</h5></div>
        
        <form class="mainForm" action="<?=ADMIN?>savePage" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset>
        <div class="widget">       
             <ul class="tabs">
                   <li><a href="#tab1">Srpski</a></li>
                   <li><a href="#tab2">Engleski</a></li>
                   <li><a href="#tab3">Ruski</a></li>
                   <li><a href="#tab4">Seo</a></li>
              </ul>
                    
              <div class="tab_container">
              			
              		<input type="hidden" name="id" value="<?=$page->id?>"/>
              		
                    <!-- Content of tab1 -->
                    <div id="tab1" class="tab_content">
                    	<div class="rowElem">
                            <label>Active:</label> 
                            <div class="formRight">
                                <input type="checkbox" name="active" <?php if($page->active == 1){echo 'checked';}?>/>
                            </div>
                            <div class="fix"></div>
                        </div>

                        <div class="rowElem noborder">
                            <label>pg_name:</label>
                            <div class="formRight">
                                <input type="text" name="pg_name" value="<?=$page->pg_name?>"/>
                            </div>
                            <div class="fix"></div>
                        </div>

                        <div class="rowElem noborder">
                            <label>Naziv strane:</label>
                            <div class="formRight">
                                <input type="text" name="name_sr" value="<?=$page->name_sr?>"/>
                            </div>
                            <div class="fix"></div>
                        </div>

                        <div class="rowElem" >
                            <div class="formRight" style="width: 100%">
                                <div class="widget" style="margin-top: 5px">
                                <div class="head"><h5 class="iPencil">Sadržaj strane</h5></div>
                                <textarea class="wysiwyg" name="desc_sr" rows="5" cols="" >
                                    <?=$page->desc_sr?>
                                </textarea
                                ></div>
                            </div>
                            <div class="fix"></div>
                        </div>   



      
                        
                    </div>  
                    <!-- End - tab1 --> 
                                         
                                         
                    <!-- Content of tab2 -->
                    <div id="tab2" class="tab_content">                   
                   		<div class="rowElem noborder">
                    		<label>Naziv strane:</label>
                    		<div class="formRight">
                    			<input type="text" name="name_en" value="<?=$page->name_en?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div> 
                    	<div class="rowElem" >
                    		<div class="formRight" style="width: 100%">
                    			<div class="widget" style="margin-top: 5px">
                                    <div class="head"><h5 class="iPencil">Sadržaj strane</h5></div>
                                    <textarea class="wysiwyg" name="desc_en" rows="5" cols="" >
                    				<?=$page->desc_en?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="fix"></div>
                    	</div>                  	
                    	
                    </div>
                    <!-- End - tab2 --> 
                    
                    <!-- Content of tab3 -->
                    <div id="tab3" class="tab_content">                   
                        
                        <div class="rowElem noborder">
                            <label>Naziv strane:</label>
                            <div class="formRight">
                                <input type="text" name="name_ru" value="<?=$page->name_ru?>"/>
                            </div>
                            <div class="fix"></div>
                        </div> 
                        
                        <div class="rowElem" >
                            <div class="formRight" style="width: 100%">
                                <div class="widget" style="margin-top: 5px">
                                <div class="head"><h5 class="iPencil">Sadržaj strane</h5></div>
                                <textarea class="wysiwyg" name="desc_ru" rows="5" cols="" ><?=$page->desc_ru?> </textarea
                                >
                            </div>
                            </div>
                            <div class="fix"></div>
                        </div>                      
                        
                    </div>                    

                    <!-- End - tab3 --> 
                    
                    <!-- Content of tab4 -->
                    <div id="tab4" class="tab_content">                     
                    	
                        <div class="rowElem noborder">
                    		<label>Naziv strane(srpski):</label>
                    		<div class="formRight">
                    			<input type="text" name="seo_title_sr" value="<?=$page->seo_title_sr?>">
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                    	
                        <div class="rowElem noborder">
                    		<label>Naziv strane(engleski):</label>
                    		<div class="formRight">
                    			<input type="text" name="seo_title_en" value="<?=$page->seo_title_en?>">
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                        
                        <div class="rowElem noborder">
                            <label>Naziv strane(ruski):</label>
                            <div class="formRight">
                                <input type="text" name="seo_title_ru" value="<?=$page->seo_title_ru?>">
                            </div>
                            <div class="fix"></div>
                        </div>

                        <div class="rowElem">
                    		<label>Opis strane za pretraživače(srpski):</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_desc_sr" ><?=$page->seo_desc_sr?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>

                    	<div class="rowElem">
                    		<label>Opis strane za pretraživače(engleski):</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_desc_en" ><?=$page->seo_desc_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>

                        <div class="rowElem">
                            <label>Opis strane za pretraživače(ruski):</label>
                            <div class="formRight">
                                <textarea rows="8" cols="" name="seo_desc_ru" ><?=$page->seo_desc_ru?>
                                </textarea></div>
                            <div class="fix"></div>
                        </div>


                    	<div class="rowElem">
                    		<label>SEO ključne reči(srpski) :</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_keywords_sr" ><?=$page->seo_keywords_sr?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>

                    	<div class="rowElem">
                    		<label>SEO ključne reči(engleski) :</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_keywords_en" ><?=$page->seo_keywords_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>

                        <div class="rowElem">
                            <label>SEO ključne reči(ruski) :</label>
                            <div class="formRight">
                                <textarea rows="8" cols="" name="seo_keywords_ru" ><?=$page->seo_keywords_ru?>
                                </textarea></div>
                            <div class="fix"></div>
                        </div>
                    
                    </div>
                    <!-- End - tab4 -->
                    
               </div>	
               <div class="fix"></div>		 
         </div>
         <!-- End - Tabs -->
		 </fieldset>
		 <input type="submit" value="Submit" class="greyishBtn submitForm" />
		 </form>

	</div>
<div class="fix"></div>
</div>


<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Eurokomerc d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>
