<?php 
require_once('../../framework/lib/setup.php');

if(isset($_POST['id']) && $_POST['id']!=0 && Administrator::find_by_id($_POST['id'])){
    $admin = Administrator::find_by_id($_POST['id']);
    $new = false;
}else{
    $admin= new Administrator();
    $new = true;
}

    foreach($admin as $key=>$value){
        // echo $_POST[$key];

        if($key!='id' && isset($_POST[$key]) && $key!= 'password'){
            $admin->$key = trim($_POST[$key]);
            //echo $admin->$key ."<br>";
        }   	
	}
	
	if(isset($_POST['password']) && $_POST['password']!= ''){
		$admin->password = md5($_POST['password']);
	}

if($admin && $admin->save()){
  $session->message('Administrator je sačuvan');
  $_SESSION['mType']= 2;

  redirect_to(ADMIN.'administratori');
}else{
  $session->message('Postoji problem. Administrator nije sačuvan');
  $_SESSION['mType']= 4;
  redirect_to(ADMIN.'administrator/'.$admin->id);
}
?>
