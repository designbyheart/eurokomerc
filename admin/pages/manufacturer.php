<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); 

if($_GET['id']==0){
    $manufacturer = new Manufacturer();
}else{
    $manufacturer = Manufacturer::find_by_id($_GET['id']);
}
?>
	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>	
    	    <?php 
            if($_GET['id']==0){
                ?>Novi proizvođač<?
            }else{
                ?>Izmena proizvođača<?
            }
          ?>
    	</h5></div>
        
        <!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->
        
		<form class="mainForm" action="<?=ADMIN?>saveManufacturer" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset>
        <div class="widget">       
             <ul class="tabs">
                   <li><a href="#tab1">Srpski</a></li>
                   <li><a href="#tab2">Engleski</a></li>
                   <li><a href="#tab3">Seo</a></li>
              </ul>
                    
              <div class="tab_container">
              			
              		<input type="hidden" name="id" value="<?=$manufacturer->id?>"/>
              		
                    <!-- Content of tab1 -->
                    <div id="tab1" class="tab_content">
                    	<div class="rowElem noborder">
                    		<label>Naziv proizvođača:</label>
                    		<div class="formRight">
                    			<input type="text" name="name_sr" value="<?=$manufacturer->name_sr?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                            <?php if($manufacturer->logo!=''){ ?>
                                <img src="<?=SITE_ROOT?>images/gallery/thumbsM/<?=$manufacturer->logo?>" alt="" class="manufactureLogo">
                            <?php } ?>
                            <label>Upload logo  :</label>
	                        <div class="formRight">
	                            <input type="file" name="logo" class="fileInput" id="fileInput" />
	                        </div>
	                        <div class="fix"></div>
                   		</div>
                   		<div class="rowElem noborder">
                    		<label>Zemlja proizvođača:</label>
                    		<div class="formRight">
                    			<input type="text" name="country_sr" value="<?=$manufacturer->country_sr?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>             	
                    	<div class="rowElem noborder">
                    		<label>Url:</label>
                    		<div class="formRight">
                    			<input type="text" name="url" value="<?=$manufacturer->url?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>                        	
	                    <div class="rowElem">
	                        <label>Tip proizvođača:</label>
	                        <div class="formRight">
	                        <select name="type" >
	                            <option value="3">Izaberi tip </option>
					          		<option value="0" <?php if($manufacturer->type == 0) {echo 'selected';}?> > Proizvođač </option>
					          		<option value="1" <?php if($manufacturer->type == 1) {echo 'selected';}?> > Partner </option>
					          		<option value="2" <?php if($manufacturer->type == 2) {echo 'selected';}?> > Referenca </option>
	                        </select>
	                        </div>
	                        <div class="fix"></div>
	                    </div>                                                             	
                    	<div class="rowElem" style=" width:47%;float:left;">
	                        <label>Aktivan:</label>
                            <div class="formRight">
	                               <input type="checkbox" name="active" <?php if($manufacturer->active == 1){ echo 'checked'; } ?>/><br>
                            </div>
	                        <div class="fix"></div>
                    	</div>
                        <div class="rowElem" style="width:46%;clear:none; float:left;">
                            <label>Prikazati u banerima</label>
                            <div class="formRight">
                                <input type="checkbox" name="banner" <?php if($manufacturer->banner== 1){ echo 'checked'; } ?>/>
                            </div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem">
                            <label>Prikazati na naslovnoj strani</label>
                            <div class="formRight">
                                <input type="checkbox" name="banner_home" <?php if($manufacturer->banner_home > 0){ echo 'checked id="'.$manufacturer->banner_home.'"';} ?>>
                                <a href="<?=ADMIN?>redosled-banera" class="button" style="position:relative; top:4px">Izmena redosleda banera &gt;&gt;</a>
                            </div>
                        </div>
                        <div class="rowElem">
                    		<label>Opis proizvođača:</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="desc_sr" ><?=$manufacturer->desc_sr?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>

                    </div>  
                    <!-- End - tab1 --> 
                                         
                                         
                    <!-- Content of tab2 -->
                    <div id="tab2" class="tab_content">                   
                   		<div class="rowElem noborder">
                    		<label>Naziv proizvođača:</label>
                    		<div class="formRight">
                    			<input type="text" name="name_en" value="<?=$manufacturer->name_en?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div> 
                    	<div class="rowElem noborder">
                    		<label>Zemlja proizvođača:</label>
                    		<div class="formRight">
                    			<input type="text" name="country_en" value="<?=$manufacturer->country_en?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>Opis proizvođača:</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="desc_en" ><?=$manufacturer->desc_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>                   	
                    	
                    </div>
                    <!-- End - tab2 --> 
                    
                    
                    <!-- Content of tab3 -->
                    <div id="tab3" class="tab_content">                     
                    	<div class="rowElem noborder">
                    		<label>Naziv prozvođača(srpski):</label>
                    		<div class="formRight">
                    			<input type="text" name="seo_title_sr" value="<?=$manufacturer->seo_title_sr?>">
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem noborder">
                    		<label>Naziv proizvođača(engleski):</label>
                    		<div class="formRight">
                    			<input type="text" name="seo_title_en" value="<?=$manufacturer->seo_title_en?>">
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                        <div class="rowElem">
                    		<label>Opis strane za pretraživače(srpski):</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_desc_sr" ><?=$manufacturer->seo_desc_sr?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>Opis strane za pretraživače(engleski):</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_desc_en" ><?=$manufacturer->seo_desc_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>SEO ključne reči(srpski) :</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_keywords_sr" ><?=$manufacturer->seo_keywords_sr?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>SEO ključne reči(engleski) :</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_keywords_en" ><?=$manufacturer->seo_keywords_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    
                    </div>
                    <!-- End - tab3 -->
                    
               </div>	
               <div class="fix"></div>		 
         </div>
         <!-- End - Tabs -->
		 </fieldset>
		 <input type="submit" value="Submit" class="greyishBtn submitForm" />
		 </form>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Eurokomerc d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>
    	