<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); ?>

	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>Dashboard</h5></div>

        <div class="stats">
        <?php $sum_prod = Product::count_active();
        	  $sum_cat = Category::count_active();
        	  
        ?>
        	<ul>
            	<li><a href="<?=ADMIN_ROOT?>proizvodi" class="count grey" title=""><?=$sum_prod?></a><span>Aktivnih <br>Proizvoda </span></li>
                <li><a href="<?=ADMIN_ROOT?>kategorije" class="count grey" title=""><?=$sum_cat?></a><span>Aktivnih<br>Kategorija</span></li>
            </ul>
            <div class="fix"></div>
        </div>
        <?php $q = Quote::getQuote();
            if(!$q){
                $q = new Quote();
            }
        ?>
        <div class="widget"> 
            <form action="<?=ADMIN?>saveQuote" method="POST" class="mainForm">
                <input class="id" type="hidden" name="id" value="<?=$q->id?>">
                <ul class="tabs">
                    <li class="activeTab">
                        <a href="#tab3">Uvodni tekst na naslovnoj strani - SRPSKI</a>
                    </li>
                    <li class="">
                        <a href="#tab4">ENGLESKI</a>
                    </li>
                    <li><a href="#tab5">RUSKI</a></li>
                </ul>
                <div class="tab_container">
                    <div id="tab3" class="tab_content" style="display: block; ">
                       <div class="rowElem noborder">
                           <label>Glavni tekst:</label>
                           <div class="formRight"><textarea  name="headline_sr" class="wysiwyg"><?=$q->headline_sr?></textarea></div>
                           <div class="formRight"><a href="<?=ADMIN?>baneri-naslovna">Editovanje bannera partnera na naslovnoj strani</a></div>
                           <div class="fix"></div>
                       </div>
                    </div>
                    <div id="tab4" class="tab_content" style="display: none; ">
                        <div class="rowElem noborder"><label>Glavni tekst - ENG:</label><div class="formRight">
                            <textarea  name="headline_en" class="wysiwyg"><?=$q->headline_en?></textarea>
                           </div><div class="fix"></div></div>
                    </div>
                    <div id="tab5" class="tab_content" style="display: none; ">
                        <div class="rowElem noborder">
                            <label>Glavni tekst - RUSKI:</label>
                            <div class="formRight">
                                <textarea  name="headline_ru" class="wysiwyg"><?=$q->headline_ru?></textarea>
                            </div><div class="fix"></div></div>
                    </div>
                </div>  
                 <input type="submit" value="Sačuvaj" name="saveQuote" class="basicBtn mr10 ml10" style="margin:0 0 20px 217px; padding:6px 30px;">
                <div class="fix"></div>  
            </form>
        </div>

        <?php 
            //$products = Product::find_by_sql("SELECT *, (select name_sr from category where category.id = product.category) as category, (select name_sr from manufacturer where manufacturer.id = product.manufacturer) as manufacturer FROM product order by active ASC, category ASC LIMIT 10");        ?>
                            
        <!-- Dynamic table -->
        <div class="table">
            <div class="head"><h5 class="iFrames"> Poslednji dodati proizvodi </h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                    <tr>
                        <th>Naziv</th>
                        <th>Kategorija</th>
                        <th>Proizvodjac</th>
                        <th>Aktivan</th>
                    </tr>
                </thead>
                <tbody>
                	<?php 
                    $products = Product::find_by_sql("select * from product order by id DESC limit 10");
                    foreach($products as $product) {?>
                    <tr class="gradeA" <?=$product->id?>>
                        <td class="center"> <a href="<?=ADMIN?>proizvod/<?=$product->id?>"> 
                        	<strong><?=$product->name_sr?><strong></a></td>
                    	<td class="center"> <?php 
                    		if($product->category){
							$category = Category::find_by_id($product->category);
							echo $category->name_sr;
                    		}
                    	?></td>
                    	<td class="center"> </td>
                    	<td class="center">
                    	      <input type="checkbox" name="active" 
					         	  <?php if($product->active == 1){
					               		 echo 'checked = "checked"';
					              }
					              ?>
				         	  />	
                    	</td>
                    </tr>
                    <?php 
                		} ?>
                    
                </tbody>
            </table>
        </div>
        
    </div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Eurokomerc d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>
    

