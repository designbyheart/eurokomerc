<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); 

if($_GET['id']==0){
    $category = new Category();
}else{
    $category = Category::find_by_id($_GET['id']);
}
?>
<!-- Content -->
<div class="content">
  <div class="title">
    <h5>	
    <?php 
      if( $_GET['id'] == 0 ){ ?> Nova kategorija <? }
      else{ ?> Izmena kategorije <? } ?>
    </h5>
  </div>

  <form class="mainForm" action="<?=ADMIN?>saveCategory" method="post" enctype="multipart/form-data">
    <!-- Tabs -->
  <fieldset>
    <?php if($session->message()!=''){
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p  class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>

      <div class="widget">       
        <ul class="tabs">
          <li><a href="#tab1">Srpski</a></li>
          <li><a href="#tab2">Engleski</a></li>
          <li><a href="#tab3">Ruski</a></li>
            <li><a href="#tab5">Galerija</a></li>
          <li><a href="#tab4">Seo</a></li>
        </ul>

        <div class="tab_container">
          <input type="hidden" name="id" value="<?=$category->id?>"/>
          <!-- Content of tab1 -->
          <div id="tab1" class="tab_content">
            
            <div class="rowElem noborder">
              <label>Naziv kategorije:</label>
              <div class="formRight">
                <input type="text" name="name_sr" value="<?=$category->name_sr?>"/>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>Active:</label> 
              <div class="formRight">
                <input type="checkbox" name="active" <?php if($category->active == 1){ echo 'checked'; } ?> />
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>Tip kategorije:</label>
              <div class="formRight">
                  <label for="categoryType" >
                      <input type="checkbox" name="type1" value="0" <?php if($category->type_category==1){ echo 'checked';} ?>>
                      <span class="catType" style="padding:5px 0 0 10px;float:left; ">Kategorija proizvoda</span>
                  </label>
                  <label for="categoryType" class="catType" <?php if($category->type_category==2){ echo 'checked';} ?>>
                      <input type="checkbox" name="type2"  >
                      <span style="padding:5px 0 0 10px;float:left; ">Kategorija galerije                  </span>
                  </label>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>Statistika:</label>
              <div class="formRight">
                Ukupno proizvoda: <b><?php echo Product::count_by_cat($_GET['id']); ?></b> <br>
                Aktivno: <b><?php echo Product::count_by_cat_act($_GET['id']); ?></b>
              </div>
              <div class="fix"></div>
            </div>

          </div>
          <!-- End - tab1 -->

          <!-- Content of tab2 -->
          <div id="tab2" class="tab_content">                   
            <div class="rowElem noborder">
              <label>Naziv kategorije:</label>
              <div class="formRight">
                <input type="text" name="name_en" value="<?=$category->name_en?>"/>
              </div>
              <div class="fix"></div>
            </div>
          </div>
          <!-- End - tab2 -->

          <!-- Content of tab3 -->
          <div id="tab3" class="tab_content">                   
            <div class="rowElem noborder">
              <label>Naziv kategorije:</label>
              <div class="formRight">
                <input type="text" name="name_ru" value="<?=$category->name_ru?>"/>
              </div>
              <div class="fix"></div>
            </div>
          </div>
          <!-- End - tab3 -->
          <div id="tab5" class="tab_content">
              <fieldset>
                  <legend>Upload fotografije</legend>
                  <input type="hidden" name="" value="1" class="current" />
                  <input id="file_upload" class="fileInput" type="file" name="image0" />
                  <div class="fields"></div><br />
                  <button name="gallery_menu" class="uploadImg">Upload</button>
              </fieldset>
              <fieldset id="galleryImages">
                  <?php
                  $img_type = 'cat-menu';
                  require(ROOT_DIR."admin/pages/galleryImages.php");
                  ?>
              </fieldset>
          </div>
          <!-- Content of tab4 -->
          <div id="tab4" class="tab_content">                     
            
            <div class="rowElem noborder">
              <label>Naziv kategorije(srpski):</label>
              <div class="formRight">
                <input type="text" name="seo_title_sr" value="<?=$category->seo_title_sr?>" >
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem noborder">
              <label>Naziv kategorije(engleski):</label>
              <div class="formRight">
                <input type="text" name="seo_title_en" value="<?=$category->seo_title_en?>">
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem noborder">
              <label>Naziv kategorije(ruski):</label>
              <div class="formRight">
                <input type="text" name="seo_title_ru" value="<?=$category->seo_title_ru?>">
              </div>
              <div class="fix"></div>
            </div>            

            <div class="rowElem">
              <label>Opis strane za pretraživače(srpski):</label>
              <div class="formRight">
                <textarea rows="8" cols="" name="seo_desc_sr" ><?=$category->seo_desc_sr?> </textarea>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>Opis strane za pretraživače(engleski):</label>
              <div class="formRight">
                <textarea rows="8" cols="" name="seo_desc_en" ><?=$category->seo_desc_en?></textarea>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>Opis strane za pretraživače(ruski):</label>
              <div class="formRight">
                <textarea rows="8" cols="" name="seo_desc_ru" ><?=$category->seo_desc_ru?></textarea>
              </div>
              <div class="fix"></div>
            </div>            

            <div class="rowElem">
              <label>SEO ključne reči(srpski) :</label>
              <div class="formRight">
                <textarea rows="8" cols="" name="seo_keywords_sr" ><?=$category->seo_keywords_sr?></textarea>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>SEO ključne reči(engleski) :</label>
              <div class="formRight"> 
                <textarea rows="8" cols="" name="seo_keywords_en" ><?=$category->seo_keywords_en?> </textarea>
              </div>
              <div class="fix"></div>
            </div>

            <div class="rowElem">
              <label>SEO ključne reči(ruski) :</label>
              <div class="formRight"> 
                <textarea rows="8" cols="" name="seo_keywords_ru" ><?=$category->seo_keywords_ru?> </textarea>
              </div>
              <div class="fix"></div>
            </div>            

          </div>
          <!-- End - tab4 -->

        </div>	
        <div class="fix"></div>
      </div>
      <!-- End - Tabs -->

    </fieldset>
    <input type="submit" name="submit" value="Submit" class="greyishBtn submitForm" />
  </form>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Banjica promet d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


