<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); 

if($_GET['id']==0){
    $news = new News();
}else{
    $news = News::find_by_id($_GET['id']);
}
?>
	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>	
    	    <?php 
            if($_GET['id']==0){
                ?>Nova aktuelnost<?
            }else{
                ?>Izmena aktuelnosti<?
            }
          ?>
    	</h5></div>
        

        
		<form class="mainForm" action="<?=ADMIN?>saveNews" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset>
        <?php if($session->message()!=''){
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p  class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>
        <div class="widget">       
             <ul class="tabs">
                   <li><a href="#tab1">Srpski</a></li>
                   <li><a href="#tab2">Engleski</a></li>
                   <li><a href="#tab3">Ruski</a></li>
            </ul>
                    
              <div class="tab_container">
              			
              		<input type="hidden" name="id" value="<?=$news->id?>"/>
              		
                    <!-- Content of tab1 -->
                    <div id="tab1" class="tab_content">
                    	<div class="rowElem noborder">
                    		<label>Naslov:</label>
                    		<div class="formRight">
                    			<input type="text" name="title_sr" value="<?=$news->title_sr?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>       
                    	
                    	<div class="rowElem noborder">
                        <label>Datum:</label>
                        <div class="formRight">
                        	<?php 
                        	if($news->date != ''){
                        		$date = date("d-m-Y", strtotime($news->date));
                        	}else{
                        		$date = '';
                        	}
                        	?>
                            <input type="text" name="date" class="datepicker" value="<?=$date?>"/>
                        </div>
                        <div class="fix"></div>
                    </div>                  	                           
	                    	
                    	<div class="rowElem">
	                        <label>Active:</label> 
	                        <div class="formRight">
	                               <input type="checkbox" name="active" 
						         	  <?php if($news->active == 1){
						               		 echo 'checked';
						              }
					              ?>
				         	  />
	                        </div>
	                        <div class="fix"></div>
                    	</div>
                                   
                        <div class="rowElem">
                    		<label>Opis aktuelnosti:</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="content_sr" class="wysiwyg" ><?=$news->content_sr?></textarea>
                            </div>
                    		<div class="fix"></div>
                    	</div> 	                            
                    </div>  
                    <!-- End - tab1 --> 
                                         
                                         
                    <!-- Content of tab2 -->
                    <div id="tab2" class="tab_content">                   
                   		<div class="rowElem noborder">
                    		<label>Naslov:</label>
                    		<div class="formRight">
                    			<input type="text" name=title_en value="<?=$news->title_en?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>                    	                       
                    	<div class="rowElem">
                    		<label>Opis aktuelnosti:</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="content_en" class="wysiwyg" ><?=$news->content_en?></textarea>
                    		</div>
                    		<div class="fix"></div>
                    	</div>                    
                    </div>
                    <!-- End - tab2 --> 
                    
                    <!-- Content of tab3 -->
                    <div id="tab3" class="tab_content">                   
                        
                        <div class="rowElem noborder">
                            <label>Naslov:</label>
                            <div class="formRight">
                                <input type="text" name="title_ru" value="<?=$news->title_ru?>"/>
                            </div>
                            <div class="fix"></div>
                        </div>                                             
                        
                        <div class="rowElem">
                            <label>Opis aktuelnosti:</label>
                            <div class="formRight">
                                <textarea rows="8" class="wysiwyg" cols="" name="content_ru" ><?=$news->content_ru?>
                                </textarea>
                            </div>
                            <div class="fix"></div>
                        </div>                    
                    
                    </div>
                    <!-- End - tab3 --> 
                
                </div>	
                <div class="fix"></div>		 
         </div>
         <!-- End - Tabs -->
		 </fieldset>
		 <input type="submit" name="submit" value="Submit" class="greyishBtn submitForm" />
		 </form>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Eurokomerc d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


