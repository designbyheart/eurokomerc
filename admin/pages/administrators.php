<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); ?>

	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>Administratori</h5></div>
        
        <!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->
        
		<?php 
			$admins = Administrator::find_all();
		?>
		  <?php if($session->message()!=''){
		  	  echo "<br>";
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>      
        <!-- Dynamic table -->
        <div class="table">
            <div class="head"><h5 class="iFrames"> <a href="<?=ADMIN?>novi-administrator">Dodaj administratora</a></h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                    <tr>
                        <th>Ime administratora</th>
                        <th>E-mail</th>
                        <th>Telefon</th>
                        <th> &nbsp;&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                	<?php foreach($admins as $admin) {?>
                    <tr class="gradeA" <?=$admin->id?>>
                        <td class="center"> <a href="<?=ADMIN?>administrator/<?=$admin->id?>"> 
                        	<strong><?=$admin->first_name?> <?=$admin->last_name?><strong></a></td>
                    	<td class="center"> <?=$admin->e_mail?> </td>
                    	<td class="center"> <?=$admin->tel?> </td>
                    	<td class="center"> 
                    		<a href="<?=ADMIN?>administrator/<?=$admin->id?>"> Edit </a> &nbsp;&nbsp;&nbsp; 
                    		<a href="pages/deleteCategory.php?id=<?=$admin->id?>"> Delete </a>                    		
                    	</td>
                    </tr>
                    <?php 
                		} ?>
                    
                </tbody>
            </table>
        </div>
        
	</div>
<div class="fix"></div>
</div>


<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Eurokomerc d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


