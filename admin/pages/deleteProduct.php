<?php 
require_once('../../framework/lib/setup.php');

if(isset($_GET['id']) && $_GET['id']!=''){
    $product = Product::find_by_id($_GET['id']);
    
if($product->delete()){
  $session->message('Proizvod je izbrisan');
  $_SESSION['mType']= 2;  		
  redirect_to(ADMIN.'proizvodi');
  
}else{
  $session->message('Postoji problem. Proizvod nije izbrisan');
  $_SESSION['mType']= 4;  		 
  redirect_to(ADMIN.'proizvodi'); 
}
}

?>