<?php
require_once('../framework/lib/setup.php');
if (!$session->is_logged_in()) {
    redirect_to(SITE_ROOT.'admin/login.php');
}
require_once('doc/pages.php');
?>