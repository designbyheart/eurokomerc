<?php require_once('../framework/lib/setup.php'); 
if (isset($_POST['submit'])) { // Form has been submitted.
  require_once("../framework/lib/setup.php");
  $username = trim($_POST['login']);
  $password = md5($_POST['password']);
 // $password = trim($_POST['password']);
  $found_user = Administrator::authenticate($username, $password);
  if ($found_user) {
    $session->login($found_user);
		//log_action('Login', "{$found_user->username} logged in.");
    redirect_to("index.php");
  } else {
    // username/password combo was not found in the database
    $message = "Pogrešno korisničko ime ili lozinka. Pokušajte ponovo.";
  }
} else { // Form has not been submitted.
  $username = "";
  $password = "";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
<title>Login</title>

<link href="<?=ADMIN?>/doc/css/login-main.css" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Cuprum" rel="stylesheet" type="text/css" />

</head>

<body>

<!-- Top navigation bar -->
<div id="topNav">
    <div class="fixed">
        <div class="wrapper">
            <div class="backTo"><a href="<?=SITE_ROOT?>" title=""><img src="<?=ADMIN?>/doc/images/icons/topnav/mainWebsite.png" alt="" /><span>Sajt</span></a></div>
            <div class="userNav">
<!--                <ul>-->
<!--                    <li><a href="#" title=""><img src="<?=ADMIN?>/doc/images/icons/topnav/register.png" alt="" /><span>Register</span></a></li>-->
<!--                    <li><a href="#" title=""><img src="images/icons/topnav/contactAdmin.png" alt="" /><span>Contact admin</span></a></li>-->
<!--                    <li><a href="#" title=""><img src="images/icons/topnav/help.png" alt="" /><span>Help</span></a></li>-->
<!--                </ul>-->
            </div>
            <div class="fix"></div>
        </div>
    </div>
</div>

<!-- Login form area -->
<div class="loginWrapper">
	<div class="loginLogo"><img src="<?=ADMIN?>doc/images/eurokomerc-logo.png" alt=""  style="position:relative; top:-30px"/></div>
    <div class="loginPanel">
        <div class="head"><h5 class="iUser">Login</h5></div>
        <form action="#" method="post" id="valid" class="mainForm">
            <fieldset>
                <div class="loginRow noborder">
                    <label for="req1">Username:</label>
                    <div class="loginInput"><input type="text" name="login" class="validate[required]" id="req1" /></div>
                    <div class="fix"></div>
                </div>
                
                <div class="loginRow">
                    <label for="req2">Password:</label>
                    <div class="loginInput"><input type="password" name="password" class="validate[required]" id="req2" /></div>
                    <div class="fix"></div>
                </div>
                
                <div class="loginRow">
<!--                    <div class="rememberMe"><input type="checkbox" id="check2" name="chbox" /><label>Remember me</label></div>-->
                    <input type="submit" name="submit" value="Log me in" class="greyishBtn submitForm" />
                    <div class="fix"></div>
                </div>
            </fieldset>
        </form>
    </div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	    	<span>&#169; Copyright <?=date("Y", time())?> Banjica promet d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>
    
<script src="<?=ADMIN?>/doc/js/jquery-1.4.4.js" type="text/javascript"></script>

<script type="text/javascript" src="<?=ADMIN?>/doc/js/spinner/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/spinner/ui.spinner.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script> 

<script type="text/javascript" src="<?=ADMIN?>/doc/js/fileManager/elfinder.min.js"></script>

<script type="text/javascript" src="<?=ADMIN?>/doc/js/wysiwyg/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/wysiwyg/wysiwyg.image.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/wysiwyg/wysiwyg.link.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/wysiwyg/wysiwyg.table.js"></script>

<script type="text/javascript" src="<?=ADMIN?>/doc/js/flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/flot/jquery.flot.resize.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/flot/excanvas.min.js"></script>

<script type="text/javascript" src="<?=ADMIN?>/doc/js/dataTables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/dataTables/colResizable.min.js"></script>

<script type="text/javascript" src="<?=ADMIN?>/doc/js/forms/forms.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/forms/autogrowtextarea.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/forms/autotab.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/forms/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/forms/jquery.validationEngine.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/forms/jquery.dualListBox.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/forms/jquery.filestyle.js"></script>

<script type="text/javascript" src="<?=ADMIN?>/doc/js/colorPicker/colorpicker.js"></script>

<script type="text/javascript" src="<?=ADMIN?>/doc/js/uploader/plupload.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/uploader/plupload.html5.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/uploader/plupload.html4.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/uploader/jquery.plupload.queue.js"></script>

<script type="text/javascript" src="<?=ADMIN?>/doc/js/ui/progress.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/ui/jquery.jgrowl.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/ui/jquery.tipsy.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/ui/jquery.alerts.js"></script>

<script type="text/javascript" src="<?=ADMIN?>/doc/js/jBreadCrumb.1.1.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/cal.min.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/jquery.smartWizard.min.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/jquery.collapsible.min.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/jquery.ToTop.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/jquery.listnav.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/jquery.sourcerer.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/jquery.timeentry.min.js"></script>
<script type="text/javascript" src="<?=ADMIN?>/doc/js/jquery.prettyPhoto.js"></script>

<script type="text/javascript" src="<?=ADMIN?>/doc/js/custom.js"></script>
    </div>
</div>

</body>
</html>
