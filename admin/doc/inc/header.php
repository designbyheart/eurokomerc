<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
		<title>Eurokomerc Administracija - <?=str_replace('/', ' ', ucfirst(str_replace('/admin/', '', $_SERVER['REQUEST_URI'])))?></title>
		<meta name="description" content="OVo je description">
		  <meta keywords="keywords" >
		
		<link rel="stylesheet" href="<?=ADMIN?>/doc/css/reset.css" type="text/css">
		<link rel="stylesheet" href="<?=ADMIN?>/doc/css/dataTable.css" type="text/css">
		<link rel="stylesheet" href="<?=ADMIN?>/doc/css/colResizable.min.js" type="text/css">
		<link rel="stylesheet" href="<?=ADMIN?>/doc/css/main.css" type="text/css">
		<link rel="stylesheet" href="<?=ADMIN?>/doc/css/ui_custom.css" type="text/css">
<!--		<link rel="stylesheet" href="<?=ADMIN?>/doc/css/gallery.css" type="text/css">-->
		
		
		<link href='http://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css' />
	</head>
	<body>
	
		<!-- Top navigation bar -->
		<div id="topNav">
		    <div class="fixed">
		        <div class="wrapper">
		        <?php $admin = Administrator::find_by_id($session->user_id);?>
		            <div class="welcome"><a href="#" title=""><img src="<?=ADMIN?>doc/images/userPic.png" alt="" /></a><span>Dobro došli <?=ucfirst($admin->first_name)?> <?=ucfirst($admin->last_name)?></span></div>
		            <div class="userNav">
		                <ul>
<!--		                    <li><a href="#" title=""><img src="<?=ADMIN?>doc/images/icons/topnav/profile.png" alt="" /><span>Profile</span></a></li>-->
<!--		                    <li><a href="#" title=""><img src="<?=ADMIN?>doc/images/icons/topnav/tasks.png" alt="" /><span>Tasks</span></a></li>-->
		                    <li class="dd">
                                <a href="<?=ADMIN?>poruke" title="">
		                    		<img src="<?=ADMIN?>doc/images/icons/topnav/messages.png" alt="" />
                                    <span>Messages</span>
                                <?php
                                    $totalMails = count(Mail::find_unread());
                                    if($totalMails>0){ ?>
		                    		<span class="numberTop"><?$totalMails?></span>
                                <?php } ?>
                                </a>

									</li>
                            <li class="dd">
                                <img src="<?=ADMIN?>doc/images/icons/topnav/contactAdmin.png" alt="" />
                                <span><a href="<?=ADMIN?>administratori">Administratori</a></span>
                            </li>
<!--		                        <ul class="menu_body">-->
<!--		                            <li><a href="#" title="">new message</a></li>-->
<!--		                            <li><a href="#" title="">inbox</a></li>-->
<!--		                            <li><a href="#" title="">outbox</a></li>-->
<!--		                            <li><a href="#" title="">trash</a></li>-->
<!--		                        </ul>-->
<!--		                    </li>-->
<!--		                    <li><a href="#" title=""><img src="<?=ADMIN?>doc/images/icons/topnav/settings.png" alt="" /><span>Settings</span></a></li>-->
		                    <li><a href="<?=ADMIN?>logout.php" title=""><img src="<?=ADMIN?>doc/images/icons/topnav/logout.png" alt="" /><span>Logout</span></a></li>
		                </ul>
		            </div>
		            <div class="fix"></div>
		        </div>
		    </div>
		</div>

		<!-- Header -->
		<div id="header" class="wrapper">
		    <div class="logo"><a href="<?=ADMIN?>" title=""><img style="width: 210px" src="<?=ADMIN?>doc/images/eurokomerc-logo.png" alt="" /></a></div>
		    <div class="middleNav">
		    	<ul>
                    <?php if($totalMails>0){ ?>
		        	    <li class="iMes"><a href="<?=ADMIN?>poruke" title=""><span>Nova poruka</span></a><span class="numberMiddle"><?=$totalMails;?></span></li>
                    <?php } ?>
            <!--
   <li class="iStat"><a href="#" title=""><span>Statistics</span></a></li>-->
<!--		            <li class="iUser"><a href="#" title=""><span>User list</span></a></li>-->
                    <li class="iBell"><a href="<?=ADMIN?>aktuelnosti" title=""><span>Aktuelnosti</span></a></li>
                    <li class="iFrames"><a href="<?=ADMIN?>opste_strane" title=""><span>Opšte strane</span></a></li>

		        </ul>
		    </div>
		    <div class="fix"></div>
		</div>


		<!-- Content wrapper -->
		<div class="wrapper">


