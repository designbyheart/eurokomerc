<?php
/**
Setting active item in left menu
 @param page name, link name: $selPage 
 @returns activeClass
 */
function activeMenu($selPage){
	global $page;
	if($page ==$selPage){
		return 'class="active"';
	}
}
/**
type:
1: proizvodjac
2: referenca 
 @param page name, link name: $selPage 
 @returns activeClass
 *
public function type(){
	switch($this->type){
		case 1:
			return 'proizvodjac';
		break;
		case 2:
			return 'referenca';
		break;
	}
}

echo $partner->type();
*/
function trans($sr, $en, $ru=NULL){
    switch($_SESSION['lang']){
        case 'sr':
            return $sr;
            break;
        case 'en':
            return $en;
            break;
        case 'ru':
            return $ru;
            break;
    }
}
function redirect_to($location = null){
	if($location != null){
		header("Location: {$location}");
		exit;
	}
}

function uploadPhoto($file, $oldFile, $W, $tSW, $tSH, $tMW, $tMH, $fName){
   // global $name;
  
  
    if($oldFile !='' && file_exists(ROOT_DIR.'images/gallery/'.$oldFile)){
      unlink(ROOT_DIR.'images/gallery/'.$oldFile);
      }
      if($oldFile !='' && file_exists(ROOT_DIR.'images/gallery/thumbsS/'.$oldFile)){
        unlink(ROOT_DIR.'images/gallery/thumbsS/'.$oldFile);
      }
      if($oldFile !='' && file_exists(ROOT_DIR.'images/gallery/thumbsM/'.$oldFile)){
        unlink(ROOT_DIR.'images/gallery/thumbsM/'.$oldFile);
      }
      move_uploaded_file($file["tmp_name"], ROOT_DIR."images/gallery/".$fName);
		
    $targetPath = ROOT_DIR.'images/gallery/';
  //  echo "<img src=".$targetPath.$fName;
	  $targetFile =  str_replace('//','/',$targetPath). $fName;
	  $targetThumbM = str_replace('//','/',$targetPath."thumbsM/") .$fName;
	  $targetThumbS = str_replace('//','/',$targetPath."thumbsS/") .$fName;
	  
	  $filename=$targetFile;
      // if(!file_exists(ROOT_DIR.'images/gallery/'.$targetFile)){
      //    return true;
      // }
		
    list($width, $height, $type, $attr) = getimagesize($targetFile);
					$t_filenameS=$targetThumbS;
					$t_filenameM=$targetThumbM;
					// Get new dimensions
					list($width, $height) = getimagesize($targetFile);

					$new_width = $W;
					//230, 245
					$thumbM_width=$tMW;
					$thumbS_width=$tSW;
					$ratio = $width/$new_width;
					$thumbS_ratio = $width/$thumbS_width;
					$thumbM_ratio = $width/$thumbM_width;
					$new_height = $height /$ratio;
					$thumbM_height =  $height /$thumbM_ratio;
					$thumbS_height =  $height /$thumbS_ratio;
					if($thumbS_height >$tSH){
						$thumbS_height =$tSH;
						$thS_ratio = $height /$tSH;
						$thumbS_width = $width/ $thS_ratio ;
					}
					if($thumbM_height >$tMH){
						$thumbM_height =$tMH;
						$thM_ratio = $height /$tMH;
						$thumbM_width = $width/ $thM_ratio ;
					}


					if($file['type']=='image/jpeg'){
					$image_p = imagecreatetruecolor($new_width, $new_height);
					$image = imagecreatefromjpeg($filename);
					imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
					// Output
					imagejpeg($image_p, $filename, 100);

					#kreiranje thumba
					$image_tM = imagecreatetruecolor($thumbM_width, $thumbM_height);
					$image_tS = imagecreatetruecolor($thumbS_width, $thumbS_height);
					$thumbS = $image;
					$thumbM = $image;
					imagecopyresampled($image_tM, $thumbM, 0, 0, 0, 0, $thumbM_width, $thumbM_height, $width, $height);
					imagecopyresampled($image_tS, $thumbS, 0, 0, 0, 0, $thumbS_width, $thumbS_height, $width, $height);

					// Output
					imagejpeg($image_tS, $t_filenameS, 100);
					imagejpeg($image_tM, $t_filenameM, 100);
                    //   return true;
                	}
                	if($file['type']=='image/gif'){
                	$image_p = imagecreatetruecolor($new_width, $new_height);
                	$trnprt_indx = imagecolortransparent($image_p);
                	if ($trnprt_indx >= 0) {

					        // Get the original image's transparent color's RGB values
					        $trnprt_color    = imagecolorsforindex($image, $trnprt_indx);

					        // Allocate the same color in the new image resource
					        $trnprt_indx    = imagecolorallocate($image_p, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);

					        // Completely fill the background of the new image with allocated color.
					        imagefill($image_p, 0, 0, $trnprt_indx);

					        // Set the background color for new image to transparent
					        imagecolortransparent($image_p, $trnprt_indx);


					}

            		//$image_p = imagecolortransparent($image_p,  0,0,0);
					$image = imagecreatefromgif($filename);
					imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
					// Output
					imagegif($image_p, $filename, 100);

					#kreiranje thumba
					$image_tS = imagecreatetruecolor($thumbS_width, $thumbS_height);
                    $image_tM = imagecreatetruecolor($thumbM_width, $thumbM_height);
					$trnprt_indx = imagecolortransparent($image);
                	if ($trnprt_indx >= 0) {

					        // Get the original image's transparent color's RGB values
					        $trnprt_color    = imagecolorsforindex($image, $trnprt_indx);

					        // Allocate the same color in the new image resource
					        $trnprt_indx    = imagecolorallocate($image_tS, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
					         $trnprt_indx    = imagecolorallocate($image_tM, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);

					        // Completely fill the background of the new image with allocated color.
					        imagefill($image_tS, 0, 0, $trnprt_indx);
					        imagefill($image_tM, 0, 0, $trnprt_indx);

					        // Set the background color for new image to transparent
					        imagecolortransparent($image_tS, $trnprt_indx);
					        imagecolortransparent($image_tM, $trnprt_indx);


					}

			//		$image_t = imagecolortransparent($thumb_width, $thumb_height,  0,0,0);
					$thumbS = $image;
					$thumbM = $image;
					imagecopyresampled($image_tS, $thumbS, 0, 0, 0, 0, $thumbS_width, $thumbS_height, $width, $height);
                    imagecopyresampled($image_tM, $thumbM, 0, 0, 0, 0, $thumbM_width, $thumbM_height, $width, $height);

					// Output
					imagegif($image_tS, $t_filenameM, 100);
					imagegif($image_tM, $t_filenameS, 100);
                    //   return true;

                	}
                	if($file['type']=='image/png'){
                	$image = @imagecreatefrompng( $filename);
            		$image_p = imagecreatetruecolor($new_width, $new_height);
            		// Turn off transparency blending (temporarily)
			        imagealphablending($image_p, false);

			        // Create a new transparent color for image
			        $color = imagecolorallocatealpha($image_p, 0, 0, 0, 127);

			        // Completely fill the background of the new image with allocated color.
			        imagefill($image_p, 0, 0, $color);

			        // Restore transparency blending

            		imageSaveAlpha($image_p, true);



					imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
					// Output
					imagepng($image_p, $filename, 9);

					#kreiranje thumba
					$image_tS = imagecreatetruecolor($thumbS_width, $thumbS_height);
					$image_tM = imagecreatetruecolor($thumbM_width, $thumbM_height);
					$thumbM = $image;
					$thumbS = $image;
					        		// Turn off transparency blending (temporarily)
			        imagealphablending($image_tM, false);
			        imagealphablending($image_tS, false);

			        // Create a new transparent color for image
			        $color = imagecolorallocatealpha($image_tS, 0, 0, 0, 127);
			         $color = imagecolorallocatealpha($image_tM, 0, 0, 0, 127);

			        // Completely fill the background of the new image with allocated color.
			        imagefill($image_tS, 0, 0, $color);
			        imagefill($image_tM, 0, 0, $color);

			        // Restore transparency blending

            		imageSaveAlpha($image_tS, true);
            		imageSaveAlpha($image_tM, true);
					@imagecopyresampled($image_tS, $thumbS, 0, 0, 0, 0, $thumbS_width, $thumbS_height, $width, $height);
					@imagecopyresampled($image_tM, $thumbM, 0, 0, 0, 0, $thumbM_width, $thumbM_height, $width, $height);

					// Output
					imagepng($image_tS, $t_filenameS, 9);
					imagepng($image_tM, $t_filenameM, 9);


                	}

}


function uploadPhotoCat($file, $oldFile, $W, $fName){
   // global $name;
  
  
    if($oldFile !='' && file_exists(ROOT_DIR.'images/gallery/'.$oldFile)){
      unlink(ROOT_DIR.'images/gallery/'.$oldFile);
      }
      move_uploaded_file($file["tmp_name"], ROOT_DIR."images/gallery/".$fName);
		
    $targetPath = ROOT_DIR.'images/gallery/';
  //  echo "<img src=".$targetPath.$fName;
	  $targetFile =  str_replace('//','/',$targetPath). $fName;
	  
	  $filename=$targetFile;
      // if(!file_exists(ROOT_DIR.'images/gallery/'.$targetFile)){
      //    return true;
      // }
		
    list($width, $height, $type, $attr) = getimagesize($targetFile);
					// Get new dimensions
					list($width, $height) = getimagesize($targetFile);

					$new_width = $W;
					//230, 245
					$ratio = $width/$new_width;
					$new_height = $height /$ratio;


					if($file['type']=='image/jpeg'){
					$image_p = imagecreatetruecolor($new_width, $new_height);
					$image = imagecreatefromjpeg($filename);
					imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
					// Output
					imagejpeg($image_p, $filename, 100);
                	}
                	
                	if($file['type']=='image/gif'){
                	$image_p = imagecreatetruecolor($new_width, $new_height);
                	$trnprt_indx = imagecolortransparent($image_p);
                	if ($trnprt_indx >= 0) {

					        // Get the original image's transparent color's RGB values
					        $trnprt_color    = imagecolorsforindex($image, $trnprt_indx);

					        // Allocate the same color in the new image resource
					        $trnprt_indx    = imagecolorallocate($image_p, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);

					        // Completely fill the background of the new image with allocated color.
					        imagefill($image_p, 0, 0, $trnprt_indx);

					        // Set the background color for new image to transparent
					        imagecolortransparent($image_p, $trnprt_indx);


					}

            		//$image_p = imagecolortransparent($image_p,  0,0,0);
					$image = imagecreatefromgif($filename);
					imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
					// Output
					imagegif($image_p, $filename, 100);
                	}
                	
                	if($file['type']=='image/png'){
                	$image = @imagecreatefrompng( $filename);
            		$image_p = imagecreatetruecolor($new_width, $new_height);
            		// Turn off transparency blending (temporarily)
			        imagealphablending($image_p, false);

			        // Create a new transparent color for image
			        $color = imagecolorallocatealpha($image_p, 0, 0, 0, 127);

			        // Completely fill the background of the new image with allocated color.
			        imagefill($image_p, 0, 0, $color);

			        // Restore transparency blending
            		imageSaveAlpha($image_p, true);


					imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
					// Output
					imagepng($image_p, $filename, 9);
				
                	}

}

function cleanFileName($file){
    $file = preg_replace("/[^a-zA-Z0-9.\/_|+ -]/", '', $file);
    $file = strtolower(trim($file, '-'));
    return $file;
}

function imageResize($fileName, $NewWith, $NewHeight)
  	{
  	  $W = $NewWith;
       $tSW = 100; $tSH = 60; $tMW=180; $tMH=100;
  	  $targetFile = ROOT_DIR.'images/gallery/'.$fileName;

  	  list($width, $height, $type, $attr) = getimagesize($targetFile);
  	 // echo imgType($type);
  	  //echo $width;
  	  if($width!=600){

    	  $targetPath = ROOT_DIR.'images/gallery/';
    	  $targetFile =  str_replace('//','/',$targetPath). $fileName;
    	  $targetThumbM = str_replace('//','/',$targetPath."thumbsM/") .$fileName;
    	  $targetThumbS = str_replace('//','/',$targetPath."thumbsS/") .$fileName;

    	  $new_width = $W;
				//230, 245
				$thumbM_width=$tMW;
				$thumbS_width=$tSW;
				$ratio = $width/$new_width;
				$thumbS_ratio = $width/$thumbS_width;
				$thumbM_ratio = $width/$thumbM_width;
				$new_height = $height /$ratio;
				$thumbM_height =  $height /$thumbM_ratio;
				$thumbS_height =  $height /$thumbS_ratio;
				if($thumbS_height >$tSH){
					$thumbS_height =$tSH;
					$thS_ratio = $height /$tSH;
					$thumbS_width = $width/ $thS_ratio ;
				}
				if($thumbM_height >$tMH){
					$thumbM_height =$tMH;
					$thM_ratio = $height /$tMH;
					$thumbM_width = $width/ $thM_ratio ;
				}
				if(imgType($type)=='jpg'){
				  $image_p = imagecreatetruecolor($new_width, $new_height);
  				$image = imagecreatefromjpeg($targetFile);
  				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
  				// Output
  				imagejpeg($image_p, $targetFile, 100);

  				#kreiranje thumba
  				$image_tM = imagecreatetruecolor($thumbM_width, $thumbM_height);
  				$image_tS = imagecreatetruecolor($thumbS_width, $thumbS_height);
  				$thumbS = $image;
  				$thumbM = $image;
  				imagecopyresampled($image_tM, $thumbM, 0, 0, 0, 0, $thumbM_width, $thumbM_height, $width, $height);
  				imagecopyresampled($image_tS, $thumbS, 0, 0, 0, 0, $thumbS_width, $thumbS_height, $width, $height);

  				// Output
  				imagejpeg($image_tS, $targetThumbS, 100);
  				imagejpeg($image_tM, $targetThumbM, 100);
				}
			}

        if($type=='gif'){
              	$image_p = imagecreatetruecolor($new_width, $new_height);
              	$trnprt_indx = imagecolortransparent($image_p);
              	if ($trnprt_indx >= 0) {

				        // Get the original image's transparent color's RGB values
				        $trnprt_color    = imagecolorsforindex($image, $trnprt_indx);

				        // Allocate the same color in the new image resource
				        $trnprt_indx    = imagecolorallocate($image_p, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);

				        // Completely fill the background of the new image with allocated color.
				        imagefill($image_p, 0, 0, $trnprt_indx);

				        // Set the background color for new image to transparent
				        imagecolortransparent($image_p, $trnprt_indx);




          		//$image_p = imagecolortransparent($image_p,  0,0,0);
				$image = imagecreatefromgif($targetFile);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				// Output
				imagegif($image_p, $targetFile, 100);

				#kreiranje thumba
				$image_tS = imagecreatetruecolor($thumbS_width, $thumbS_height);
				$trnprt_indx = imagecolortransparent($image);

        if ($trnprt_indx >= 0) {

				        // Get the original image's transparent color's RGB values
				        $trnprt_color    = imagecolorsforindex($image, $trnprt_indx);

				        // Allocate the same color in the new image resource
				        $trnprt_indx    = imagecolorallocate($image_tS, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
				         $trnprt_indx    = imagecolorallocate($image_tM, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);

				        // Completely fill the background of the new image with allocated color.
				        imagefill($image_tS, 0, 0, $trnprt_indx);
				        imagefill($image_tM, 0, 0, $trnprt_indx);

				        // Set the background color for new image to transparent
				        imagecolortransparent($image_tS, $trnprt_indx);
				        imagecolortransparent($image_tM, $trnprt_indx);


				  }

		//		$image_t = imagecolortransparent($thumb_width, $thumb_height,  0,0,0);
				$thumbS = $image;
				$thumbM = $image;
				imagecopyresampled($image_tS, $thumbS, 0, 0, 0, 0, $thumbS_width, $thumbS_height, $width, $height);
        imagecopyresampled($image_tM, $thumb, 0, 0, 0, 0, $thumbM_width, $thumbM_height, $width, $height);

				// Output
				imagegif($image_tS, $t_filenameM, 100);
				imagegif($image_tM, $t_filenameS, 100);
                  //   return true;

              	}
              	if($type=='png'){
              	$image = @imagecreatefrompng( $targetFile);
          		$image_p = imagecreatetruecolor($new_width, $new_height);
          		// Turn off transparency blending (temporarily)
		        imagealphablending($image_p, false);

		        // Create a new transparent color for image
		        $color = imagecolorallocatealpha($image_p, 0, 0, 0, 127);

		        // Completely fill the background of the new image with allocated color.
		        imagefill($image_p, 0, 0, $color);

		        // Restore transparency blending

          		imageSaveAlpha($image_p, true);



				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				// Output
				imagepng($image_p, $targetFile, 9);

				#kreiranje thumba
				$image_tS = imagecreatetruecolor($thumbS_width, $thumbS_height);
				$image_tM = imagecreatetruecolor($thumbM_width, $thumbM_height);
				$thumbM = $image;
				$thumbS = $image;
				        		// Turn off transparency blending (temporarily)
		        imagealphablending($image_tM, false);
		        imagealphablending($image_tS, false);

		        // Create a new transparent color for image
		        $color = imagecolorallocatealpha($image_tS, 0, 0, 0, 127);
		         $color = imagecolorallocatealpha($image_tM, 0, 0, 0, 127);

		        // Completely fill the background of the new image with allocated color.
		        imagefill($image_tS, 0, 0, $color);
		        imagefill($image_tM, 0, 0, $color);

		        // Restore transparency blending

          		imageSaveAlpha($image_tS, true);
          		imageSaveAlpha($image_tM, true);
				@imagecopyresampled($image_tS, $thumbS, 0, 0, 0, 0, $thumbS_width, $thumbS_height, $width, $height);
				@imagecopyresampled($image_tM, $thumbM, 0, 0, 0, 0, $thumbM_width, $thumbM_height, $width, $height);

				// Output
				imagepng($image_tS, $t_filenameS, 9);
				imagepng($image_tM, $t_filenameM, 9);

            }
  	  }
}

function imgType($id){
  switch($id){
    case 1:
      return "gif";
    break;
    case 2:
      return "jpg";
    break;
    case 3:
      return "png";
    break;
  }
}

function trunc($string, $length){
    settype($string, 'string');
    settype($length, 'integer');
    $output = '';
    for($a = 0; $a < $length AND $a < strlen($string); $a++){
        $output .= $string[$a];
    }
    if(strlen($string) > $length){
        $output .='...';
    }
    return($output);
}
function formatDate($date){
    $month = date('M ', strtotime($date));
    if(isset($_SESSION['lang']) && $_SESSION['lang']=='sr'){
        $months = array('Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust', 'Septembar', 'Oktobar', 'Novembar', 'Decembar');
        $month = $months[date("n", strtotime($date)) -1];
    }
    $date = date('d. ', strtotime($date)).$month.  date(' Y. ', strtotime($date));
    return $date;
}
function getImage($refID, $ref, $count){
    $img = Gallery::find_by_reference($refID, $ref, $count);
    print_r($img);
    if(!$img){
        $img = "defaultCat.png";
    }else{
    	if($count<2){
    		$img = array_shift($img);
    		$img = $img->file;	
    	}else{
    		$images = array();
    		foreach($img as $i){
    			$images[] = $i->file;
    		}
    		$img = $images;
    	}
    }
    return $img;
}
function getGallery($id, $type){
	$gallery = array_shift(Gallery::find_by_sql("SELECT id, file from gallery where refID = '" . $id . "' and type = '{$type}'"));
	$gallery = Gallery::find_by_sql("SELECT id, file from gallery where refID = '" . $id . "' and type = '{$type}'");
	$gallery = array_shift($gallery);
	if($gallery){
		return $gallery->file;
	}else{
		return NULL;	
	}
}
function breadCrumb($path){
	global $page;
    for($i = count($path)-1; $i>-1; $i--){
        $c= $path[$i];
        if(count($path)>1 || $page=='proizvod'){
        echo '<li><a href="'.SITE_ROOT.'kategorija/'.$c->id.'/'.urlSafe(trans($c->name_sr, $c->name_en)).'">'.	trans($c->name_sr, $c->name_en).'</a><span class="divider">/</span></li>';
        }else{
            echo '<li>'.trans($c->name_sr, $c->name_en).'</li>';
        }
    }
}

function urlSafe($str, $replace=array(), $delimiter='-') {
	setlocale(LC_ALL, 'en_US.UTF8');
	 if( !empty($replace) ) {
	  $str = str_replace((array)$replace, ' ', $str);
	 }

	 $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	 $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	 $clean = strtolower(trim($clean, '-'));
	 $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

	 return $clean;
}

function findSubCatsOptionMS($category = NULL)  //categorija sel. proizvoda
{
	$str = "<option value=\"0\">Izaberi kategoriju</option>";
	$categories = Category::find_by_sql("SELECT id, name_sr FROM category order by name_sr ASC");
	foreach ($categories as $cat) {

		if( $cat->id == $category )
//			$str .=
               echo  '<option value="'.$cat->id.'" selected >'.$cat->name_sr.'</option>';
		else
            echo  '<option value="'.$cat->id.'" >'.$cat->name_sr.'</option>';
	}
	return $str;
}

function findSubCatsOption($category = NULL){
    ?> <option value="0">Izaberi kategoriju</option>
    <?php
    $categories = Category::find_by_sql("SELECT id, name_sr FROM category order by name_sr ASC");
    foreach ($categories as $cat) : ?>
    <option value="<?=$cat->id?>"
        <?php if($category && $category->id > 0){
        if(isset($category->id) && $cat->id == $category->parent_cat){ echo 'selected';}
    }?>
        ><?=$cat->name_sr;?> [glavna]</option>
    <?php
        $sCats = Category::find_children($cat->id);
        if($sCats){
            foreach($sCats as $sC){
                if($cat->parent_id>0){
                    $inset = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;';
                }else{
                    $inset = '&nbsp;&nbsp;&rarr;';
                }
                $sel = '';
                if($sC->id == $_GET['id']){
                    $sel = ' selected';
                }
                ?>
            <option value="<?=$sC->id?>"<?=$sel?>><?=$inset.$sC->name_sr?></option>
            <?php
                $sCs = Category::find_children($sC->id);
                if($sCs){
                    foreach($sCs as $ssC){
                        if($sC->parent_id>0){
                            $inset = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;';
                        }else{
                            $inset = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;';
                        }
                        $sel = '';
                        if($ssC->id == $_GET['id']){
                            $sel = ' selected';
                        }
                        ?>
                    <option value="<?=$ssC->id?>"<?=$sel?> style="color:red"><?=$inset.$ssC->name_sr?></option>
                    <?php
                        //showing sub sub categories
                        $ssCs = Category::find_children($ssC->id);
                        if($ssCs){
                            foreach($ssCs as $ssCs){
                                $inset = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;';
                                $sel = '';
                                if($ssCs->id==$_GET['id']){
                                    $sel = ' selected';
                                }
                                ?>
                            <option value="<?=$ssCs->id?>"<?=$sel?> style="color:red"><?=$inset.$ssCs->name_sr?></option>
                            <?php
                            }
                            $sssCs = Category::find_children($ssCs->id);
                            if($sssCs){
                                foreach ($sssCs as $sssCs) {
                                    $inset = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;';
                                    $sel = '';
                                    if($sssCs->id==$_GET['id']){
                                        $sel = ' selected';
                                    }
                                    ?>
                                <option value="<?=$sssCs->id?>"<?=$sel?> style="color:red"><?=$inset.$sssCs->name_sr?></option>
                                <?php
                                }

                            }
                        }
                    }
                 } } }
    endforeach;
}
function similarProducts($product, $limit){
// <<<<<<< HEAD
    $sql = "SELECT * from product where product.category = (select id from category where name_sr = '".$product->category."') and id !=".$product->id." LIMIT ".$limit;
    $prods = Product::find_by_sql($sql);
// =======
//     $prods = Product::find_by_sql("SELECT * from product where product.name_sr !='{$product->name_sr}' AND product.category in (select id from category where category.name_sr = '{$product->category}'");
// >>>>>>> 568b584f21a2a444fe7a05bbd33ee55b6901c069
    if($prods){
        echo '<h2>'.trans("Ostali proizvodi iz ove kategorije", 'Other products in this category').'</h2>';
        echo '<div class="proizvodi">';
    foreach($prods as $p):
        $img = SITE_ROOT.'images/gallery/thumbsM/defaultCat.png';
        $gallery = getGallery($p->id, 'product');
        if($gallery){
            $img = SITE_ROOT.'images/gallery/'.$gallery;
        } ?>
    <div class="box">
                        <span class="title">
                            <a href="<?=SITE_ROOT?>proizvod/<?=$p->id?>/<?=cleanFileName(trans($p->name_sr,$p->name_en))?>">
                                <?=trans($p->name_sr,$p->name_en);?>
                            </a>
                        </span>
        <a href="<?=SITE_ROOT?>proizvod/<?=$p->id?>/<?=cleanFileName(trans($p->name_sr,$p->name_en))?>" class="img-link">
                            <span>
                                <img src="<?=$img?>" alt="<?=trans($p->name_sr, $p->name_en)?>" />
                            </span>
        </a>
        <?php //TODO:: dodati kasnije poruci link ?>
        <span class="korpa" style="display:none"><a href="#">poruči <span class="icon-shopping-cart"></span></a></span>
    </div>
    <?php endforeach;
        echo '</div>';
    }
}

function sendMail($to,$mail_subject,$template,$header, $redirectSucc, $redirectErr){
	$send = 0;
	$result = mail($to, $mail_subject, $template, $header);
	
	        if ($result) {
	            if ($redirectSucc != '') {
	              $_SESSION['message'] = trans("Poruka je uspešno poslata.",'Message is sent successfully.');
	              $_SESSION['mType']= 2;
	              redirect_to($redirectSucc);
	            }
	        } else {
	            if ($redirectErr != '') {
	               $_SESSION['message'] = trans("Poruka nije poslata. Pokusajte ponovo", 'Message has NOT been sent. Please try again');
	               $_SESSION['mType']= 4;
	               redirect_to($redirectErr);
	            }
	        }
}
function clearFileName($string, $force_lowercase = true, $anal = false) {
    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
        "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
        "â€”", "â€“", ",", "<", ">", "/", "?");
    $clean = trim(str_replace($strip, "", strip_tags($string)));
    $clean = preg_replace('/\s+/', "-", $clean);
    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
    return ($force_lowercase) ?
        (function_exists('mb_strtolower')) ?
            mb_strtolower($clean, 'UTF-8') :
            strtolower($clean) :
        $clean;
}
function makeSearch($term){
    $results = array();
    $searchProducts = Product::search($term);
    if($searchProducts){
        $results[] = array("products"=>$searchProducts);
    }
    return $results;
}
function detect_city($ip) {

    $default = 'UNKNOWN';

    if (!is_string($ip) || strlen($ip) < 1 || $ip == '127.0.0.1' || $ip == 'localhost' || $ip=='::1' ){
        $ip = '8.8.8.8';
    }

    $curlopt_useragent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)';

    $url = 'http://ipinfodb.com/ip_locator.php?ip=' . urlencode($ip);
    $ch = curl_init();

    $curl_opt = array(
        CURLOPT_FOLLOWLOCATION  => 1,
        CURLOPT_HEADER      => 0,
        CURLOPT_RETURNTRANSFER  => 1,
        CURLOPT_USERAGENT   => $curlopt_useragent,
        CURLOPT_URL       => $url,
        CURLOPT_TIMEOUT         => 1,
        CURLOPT_REFERER         => 'http://' . $_SERVER['HTTP_HOST'],
    );

    curl_setopt_array($ch, $curl_opt);

    $content = curl_exec($ch);

    if (!is_null($curl_info)) {
        $curl_info = curl_getinfo($ch);
    }

    curl_close($ch);

    if ( preg_match('{<li>City : ([^<]*)</li>}i', $content, $regs) )  {
        $city = $regs[1];
    }
    if ( preg_match('{<li>State/Province : ([^<]*)</li>}i', $content, $regs) )  {
        $state = $regs[1];
    }

    if( $city!='' && $state!='' ){
        $location = $city . ', ' . $state;
        return $location;
    }else{
        return $default;
    }
}

function textOnly($textSr, $textEn, $textRu, $stripALL = NULL, $truncateLimit=0){
    $text = '';
    switch($_SESSION['lang']){
        case 'sr':
            if($stripALL){
                $text =  strip_tags($textSr, '<p>');
            }
            $text =  strip_tags($textSr, '<b>,<li>, <ul>,<p>, <span>, <strong>');
            break;
        case 'en':
            if($stripALL){
                $text =  strip_tags($textEn, '<p>');
            }
            $text =  strip_tags($textEn, '<b>,<li>, <ul>, <strong>');
            break;
        case 'ru':
            if($stripALL){
                $text =  strip_tags($textRu, '<p>');
            }
            $text =  strip_tags($textRu, '<b>,<li>, <ul>, <strong>');
            break;
    }
    if($truncateLimit>0){
        return trunc($text, $truncateLimit);
    }
}
/**
Getting actual image for selected object from gallery
@param $g is object ID
@param $type is tyoe of the object
@param $thumb: do we need thumbs or big image
@returns activeClass
 */
function galleryImg($g,$type, $thumbs=TRUE, $limit=NULL){
    $gallery = array_shift(Gallery::find($g, $type));
    if(!$gallery){ return; }
    if(count($gallery)==1){
        return '<img src="'.SITE_ROOT.'images/gallery/thumbsM/'.$gallery->file.'"  >';
    }
    foreach($gallery as $gl){
        if($thumbs){
           return  '<img src="'.SITE_ROOT.'images/gallery/thumbsM/'.$gl->file.'"  >';
        }
        return '<img src="'.SITE_ROOT.'images/gallery/'.$gl->file.'"  >';
    }
}
function prArr($array=NULL){
    if(isset($array)){
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }
}
function clearFont($text){
    $text= str_replace("font-family: 'Lucida Grande';","", $text);
    $text = str_replace('font-size: 11px;', '', $text) ;
    return $text;
}
?>