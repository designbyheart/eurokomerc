-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 24, 2012 at 01:48 AM
-- Server version: 5.5.9
-- PHP Version: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `eurokomerc`
--

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE `manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(256) NOT NULL,
  `name_en` varchar(256) NOT NULL,
  `logo` varchar(256) NOT NULL,
  `country_sr` varchar(256) NOT NULL,
  `country_en` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `type` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `desc_sr` longtext NOT NULL,
  `desc_en` longtext NOT NULL,
  `seo_title_sr` varchar(256) NOT NULL,
  `seo_title_en` varchar(256) NOT NULL,
  `seo_desc_sr` text NOT NULL,
  `seo_desc_en` text NOT NULL,
  `seo_keywords_sr` text NOT NULL,
  `seo_keywords_en` text NOT NULL,
  `banner` int(1) NOT NULL,
  `banner_home` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `manufacturer`
--


-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(256) NOT NULL,
  `name_en` varchar(256) NOT NULL,
  `name_ru` varchar(256) NOT NULL,
  `category` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `desc_sr` longtext NOT NULL,
  `desc_en` longtext NOT NULL,
  `desc_ru` longtext NOT NULL,
  `seo_title_sr` varchar(256) NOT NULL,
  `seo_title_en` varchar(256) NOT NULL,
  `seo_title_ru` varchar(256) NOT NULL,
  `seo_desc_sr` text NOT NULL,
  `seo_desc_en` text NOT NULL,
  `seo_desc_ru` text NOT NULL,
  `seo_keywords_sr` text NOT NULL,
  `seo_keywords_en` text NOT NULL,
  `seo_keywords_ru` text NOT NULL,
  `updated` int(15) NOT NULL,
  `created` int(15) NOT NULL,
  PRIMARY KEY (`id`)
  ) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--


-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE `quotes` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `headline_sr` text NOT NULL,
  `headline_en` text NOT NULL,
  `headline_ru` text NOT NULL,
  `subline_sr` text NOT NULL,
  `subline_en` text NOT NULL,
  `subline_ru` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;


CREATE TABLE `category` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`name_sr` varchar(256) NOT NULL,
`name_en` varchar(256) NOT NULL,
`name_ru` varchar(256) NOT NULL,
`active` int(11) NOT NULL,
`seo_title_sr` varchar(256) NOT NULL,
`seo_title_en` varchar(256) NOT NULL,
`seo_title_ru` varchar(256) NOT NULL,
`seo_desc_sr` text NOT NULL,
`seo_desc_en` text NOT NULL,
`seo_desc_ru` text NOT NULL,
`seo_keywords_sr` text NOT NULL,
`seo_keywords_en` text NOT NULL,
`seo_keywords_ru` text NOT NULL,
`type_category` int(11) NOT NULL, -- tip kategorije da li je kategorija proizvoda ili kategorija galerije --
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;


CREATE TABLE `gallery` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`img` varchar(256) NOT NULL,
`refID` int(11) NOT NULL,
`type` varchar(256) NOT NULL, -- kako se zovu tabele --
`file` varchar(256) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE `general_page` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`name_sr` varchar(256) NOT NULL,
`name_en` varchar(256) NOT NULL,
`name_ru` varchar(256) NOT NULL,
`active` int(11) NOT NULL,
`desc_sr` longtext NOT NULL,
`desc_en` longtext NOT NULL,
`desc_ru` longtext NOT NULL,
`seo_title_sr` varchar(256) NOT NULL,
`seo_title_en` varchar(256) NOT NULL,
`seo_title_ru` varchar(256) NOT NULL,
`seo_desc_sr` text NOT NULL,
`seo_desc_en` text NOT NULL,
`seo_desc_ru` text NOT NULL,
`seo_keywords_sr` text NOT NULL,
`seo_keywords_en` text NOT NULL,
`seo_keywords_ru` text NOT NULL,
`pg_name` varchar(128) CHARACTER SET utf8 NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE `mail` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`title` varchar(256) NOT NULL,
`reftitle` varchar(256) NOT NULL,
`email` varchar(256) NOT NULL,
`name` varchar(256) NOT NULL,
`date` date NOT NULL,
`phone` varchar(128) NOT NULL,
`status` int(11) NOT NULL,
`messageContent` text NOT NULL,
`refMessageContent` text NOT NULL,
`receiver` varchar(128) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE `news` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`title_sr` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
`title_en` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
`title_ru` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
`content_sr` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
`content_en` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
`content_ru` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
`date` int(15) NOT NULL,
`ordering` int(11) NOT NULL,
`active` int(1) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE `reference` (
`id` int(15) NOT NULL AUTO_INCREMENT,
`headline_sr` text DEFAULT NULL,
`headline_en` text DEFAULT NULL,
`headline_ru` text DEFAULT NULL,
`subline_sr` text DEFAULT NULL,
`subline_en` text DEFAULT NULL,
`subline_ru` text DEFAULT NULL,
`active` int(1) NOT NULL,
`file` varchar(128) DEFAULT NULL,
`url` varchar(256) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;