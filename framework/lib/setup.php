<?php
date_default_timezone_set('Europe/Belgrade');

switch($_SERVER["SERVER_NAME"]){
	case "eurokomerc": 
		defined("SITE_ROOT")?null:define("SITE_ROOT", "http://".$_SERVER["SERVER_NAME"]."/"); 
		defined("ROOT_DIR")?null:define("ROOT_DIR", $_SERVER["DOCUMENT_ROOT"]."/");
	break;
	case "designbyheart.net": 
		defined("SITE_ROOT")?null:define("SITE_ROOT", "http://".$_SERVER["SERVER_NAME"]."/eurokomerc/"); 
		defined("ROOT_DIR")?null:define("ROOT_DIR", $_SERVER["DOCUMENT_ROOT"]."/eurokomerc/");
	break;
    case "eurokomerc.com":
    case "www.eurokomerc.com":
        defined("SITE_ROOT")?null:define("SITE_ROOT", "http://".$_SERVER["SERVER_NAME"]."/");
        defined("ROOT_DIR")?null:define("ROOT_DIR", $_SERVER["DOCUMENT_ROOT"]."/");
        break;
	case "localhost": 
	//default;
		defined("SITE_ROOT")?null:define("SITE_ROOT", "http://".$_SERVER["SERVER_NAME"]."/tenkovi/"); 
		defined("ROOT_DIR")?null:define("ROOT_DIR", $_SERVER["DOCUMENT_ROOT"]."/tenkovi/");
	break;

}

defined("LIBS_DIR")?null:define("LIBS_DIR", ROOT_DIR."/framework/lib/");
defined("_CLASS")?null:define("_CLASS", ROOT_DIR."/framework/classes/");

defined("INC")?null:define("INC", ROOT_DIR."framework/inc/");
defined("MOD")?null:define("MOD", ROOT_DIR."framework/models/");
defined("VIEW")?null:define("VIEW", ROOT_DIR."framework/views/");
defined("ADMIN_ROOT")?null:define("ADMIN_ROOT", ROOT_DIR."admin/");

defined("PAGE_404")?null:define("PAGE_404", INC."404.php");

defined("ADMIN")?null:define("ADMIN", SITE_ROOT."admin/");

defined("BANNER")?null:define("BANNER", SITE_ROOT."images/gallery/thumbsM/");

defined("E_TEMP")?null:define("E_TEMP", ROOT_DIR.'framework/mailTemp/full_width.php');
defined("E_TEMP_ROOT")?null:define("E_TEMP_ROOT", SITE_ROOT.'framework/mailTemp/images/');

//config, funcion_lib, sesion, database, database_object, 
require_once(LIBS_DIR."config.php");
require_once(LIBS_DIR."function_lib.php");
require_once(LIBS_DIR."session.php");
require_once(LIBS_DIR."database.php");
require_once(LIBS_DIR."database_object.php");

 require_once(_CLASS."product.php");
 require_once(_CLASS."category.php");
 //require_once(_CLASS."manufacturer.php");
 require_once(_CLASS."general_page.php");
 require_once(_CLASS."gallery.php");
 require_once(_CLASS."administrator.php");
 require_once(_CLASS."news.php");
 require_once(_CLASS."mail.php");
 require_once(_CLASS."cache.php");
 require_once(_CLASS.'quotes.php');
 require_once(_CLASS.'reference.php');
require_once(_CLASS.'certificate.php');
?>