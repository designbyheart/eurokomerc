<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 4/19/12
 * Time: 2:16 AM
 * To change this template use File | Settings | File Templates.
 */
if(!isset($_GET['message'])){ ?>

    <form action="#" class="contact" method="post">
        <label for="name"><?=trans('Ime','Name')?>
            <input name="name" type="text">
            <span class="required reqname">Obavezno polje</span>
        </label>
        <label for="email">E-mail
            <input type="text" name="email">
            <span class="required reqemail">Obavezno polje</span>
        </label>
        <label for="message"><?=trans('Text poruke','Message')?>
            <textarea name="message"></textarea>
            <span class="required reqmessage">Obavezno polje</span>
        </label>
        <input type="submit" name="sendMessage" id="sendMessage" class="btn details" value="<?=trans('Pošalji poruku','Send message')?>">
    </form>
    <?php }else{
    switch($_GET['message']){
        case 'poruka-poslata':
        case 'message-sent':
            echo '<h2>'.trans('Poruka poslata', 'Message sent').'</h2>';
            echo '<p>'.trans('Vaša poruka je uspešno poslata.<br><br>Hvala na poverenju.<br>Eurokomerc tim', 'Your message has been successfully sent. <br> Thank you for your trust. <br> Eurokomerc team').'</p>';
            break;
        case 'poruka-nije-poslata':
        case 'message-not-sent':
            echo '<h2>'.trans('Poruka nije poslata', 'Message has not sent').'</h2>';
            echo '<p>'.trans('Došlo je do greške u slanju vaše poruke.<br>Molimo, pokušajte ponovo', 'There was an error in sending your message. <br> Please try again').'</p>';
            break;

    }
}?>