<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 5/29/12
 * Time: 12:56 AM
 * To change this template use File | Settings | File Templates.
 */
?>

<div class="sidebar">
    <h3>Kategorije</h3>
    <ul>
        <?php
        $type = 1;
        $galType='kategorija';
        if($page=='galerija' || $page=='galerijaDetails'){
            $type= 2;
            $galType='galerija';
        }
        $categories = Category::find_active($type);
        $act = '';
        foreach($categories as $c):
            if(isset($_GET['id']) && $_GET['id']==$c->id){
                $act = 'class="active"';
            }else{
                $act='';
            }
            ?>
        <li><a href="<?=SITE_ROOT.$galType?>/<?=$c->id?>/<?=urlSafe(trans($c->name_sr, $c->name_en, $c->name_ru))?>" <?=$act?>><?=trans($c->name_sr, $c->name_en, $c->name_ru)?></a></li>
        <?php endforeach; ?>
    </ul>
</div>