<!DOCTYPE HTML>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Eurokomerc</title>
	<link rel="stylesheet" href="<?=SITE_ROOT?>assets/css/style.css">
</head>
<body>
	<header id="header" class="">
        <a class="logo" href="<?=SITE_ROOT?>">Eurokomerc</a>
        <nav class="lang">
            <a href="#">English</a>
            <a href="#">Russian</a>
        </nav>
        <form action="<?=SITE_ROOT?>pretraga" method="POST" class="searchForm">
            <input type="text" name="searchF" id="searchF" placeholder="<?=trans("Unesite pojam za pretragu...", "Enter search term..."); ?>">
            <button><?=trans('Pronađi', 'Search');?></button>
        </form>
	</header>
	<section class="content">
        <section class="container">
             <?php require_once(INC.'mainMenu.php'); ?>