<nav class="mainNav">
    <a href="<?=SITE_ROOT?>" <?=activeMenu('home')?>><?=trans('Naslovna', 'Home page');?></a>
    <a href="<?=SITE_ROOT?>proizvodi" <?=activeMenu('proizvodi')?>><?=trans('Proizvodi', 'Products');?></a>
	<a href="<?=SITE_ROOT?>galerija" <?=activeMenu('galerija')?>><?=trans('Galerija', 'Gallery');?></a>
	<a href="<?=SITE_ROOT?>aktuelnosti" <?=activeMenu('aktuelnosti')?>><?=trans('Aktuelnosti', 'Actualities');?></a>
	<a href="<?=SITE_ROOT?>o-nama" <?=activeMenu('o-nama')?>><?=trans('O nama', 'About us');?></a>
	<a href="<?=SITE_ROOT?>sertifikati" <?=activeMenu('sertifikati')?>><?=trans('Sertifikati', 'Sertificates');?></a>
	<a href="<?=SITE_ROOT?>reference" <?=activeMenu('reference')?>><?=trans('Reference', 'Reference');?></a>
	<a href="<?=SITE_ROOT?>kontakt" <?=activeMenu('kontakt')?>><?=trans('Kontakt', 'Contact');?></a>
</nav>