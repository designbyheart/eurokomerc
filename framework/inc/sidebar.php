<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 5/29/12
 * Time: 12:56 AM
 * To change this template use File | Settings | File Templates.
 */
?>

<div class="sidebar">
    <h3>Eurokomerc</h3>
    <ul>
        <li><a href="<?=SITE_ROOT?>o-nama" <?=activeMenu('o-nama')?>>O nama</a></li>
        <li><a href="<?=SITE_ROOT?>kontakt" <?=activeMenu('kontakt')?>>Kontakt</a></li>
        <li><a href="<?=SITE_ROOT?>reference" <?=activeMenu('reference')?>>Reference</a></li>
    </ul>
    <h3><?=trans("Popularni proizvodi", "Popular Products")?></h3>
    <ul class="productList">
        <?php $pr = Product::findPopularRandom();
        if($pr && count($pr)>1){
            foreach ($pr as $p){ ?>
            <li>
                <a href="<?=SITE_ROOT?>proizvod/<?=$p->id?>/<?=urlSafe(trans($p->name_sr, $p->name_en, $p->name_ru))?>" <?php if($page=='proizvod' && $_GET['id']==$p->id){echo 'class="active" ';}?>>
                    <?=trans($p->name_sr, $p->name_en, $p->name_ru)?>
                </a>
            </li>
        <?php }
            }else{
            if($pr) { $p = $pr; }
                ?>
                <li><a href="<?=SITE_ROOT?>proizvod/<?=$p->id?>/<?=urlSafe(trans($p->name_sr, $p->name_en, $p->name_ru))?>" <?php if($page=='proizvod' && $_GET['id']==$p->id){echo 'class="active" ';}?>><?=trans($p->name_sr, $p->name_en, $p->name_ru)?></a></li>
                <?php
            } ?>
    </ul>

</div>