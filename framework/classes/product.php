<?php
require_once(LIBS_DIR.'database.php');

class Product extends DatabaseObject {

	protected static $table_name = "product";
	protected static $db_fields = array( 'id', 'name_sr', 'name_en', 'name_ru', 'category', 'active', 'status', 'desc_sr', 'desc_en', 'desc_ru', 'seo_title_sr', 'seo_title_en', 'seo_title_ru', 'seo_desc_sr', 'seo_desc_en', 'seo_desc_ru', 'seo_keywords_sr', 'seo_keywords_en', 'seo_keywords_ru', 'updated', 'created' );
	
    public $id;   
    public $name_sr;
    public $name_en;       
    public $name_ru;       
    public $category;       
    public $active;      
    public $status;        
    public $desc_sr;        
    public $desc_en;        
    public $desc_ru;        
    public $seo_title_sr;
    public $seo_title_en;  
    public $seo_title_ru;  
    public $seo_desc_sr;  
    public $seo_desc_en;   
    public $seo_desc_ru;   
    public $seo_keywords_sr;
    public $seo_keywords_en;
    public $seo_keywords_ru;
    public $updated;
    public $created;       


    //object specific methods
    public static function status(){
        switch($this->status){
            case 0:
                return '';
            break;
            case 1:
                return 'Promo';
                break;
        }
    }
	// Common Database Methods
    public static function find_all() {
        return self::find_by_sql("SELECT * FROM " . self::$table_name);
    }
    public  static function search($term){
        return self::find_by_sql("SELECT distinct(id) FROM " . self::$table_name." where name_sr like '%{$term}%' OR  name_en like '%{$term}%' OR  desc_en like '%{$term}%' OR  desc_sr like '%{$term}%' OR  plD like '%{$term}%'");
    }
        public static function  find_latest($count){
        return self::find_by_sql("SELECT distinct id, name_sr, name_en FROM " . self::$table_name." where active = 1 and name_sr!='naslovna' order by created DESC, updated DESC limit ".$count);
    }

    public static function  find_promo_latest($count){
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name." where active = 1 and promo = 1 and name_sr!='naslovna' order by created DESC, updated DESC limit ".$count);
        if(empty($result_array)){
            $result_array = self::find_latest($count);
        }
        return $result_array;
    }
    public static function findPopularRandom($count = 1){
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name." where active = 1  and name_sr!='naslovna' and status = 1 order by RAND() limit ".$count);
        if(empty($result_array)){
            $result_array = self::find_latest($count);
        }
        if(count($result_array)<2){
            return array_shift($result_array);
        }
        return $result_array;
    }

    public static function find_random(){
        return array_shift(self::find_by_sql("SELECT * from " . self::$table_name." where active='1' order by RAND() limit 1"));
    }
    public static function find_product($id = 0) {
        $result_array =  array();
        $lang = $_SESSION['lang'];
        if($lang=='en'){
            $result_array = self::find_by_sql("SELECT name_en, plD, size_en, power_en,active, promo, status, characteristics_en, desc_en,
            power_type_en, seo_keywords_en, seo_title_en, seo_desc_en,(select name_en from category where category.id = product.category) as category FROM " . self::$table_name . " WHERE id={$id} LIMIT 1");
        }else{
            $result_array = self::find_by_sql("SELECT name_sr, plD, size_sr, power_sr,active, promo, status, characteristics_sr, desc_sr,
            power_type_sr, seo_keywords_sr, seo_title_sr, seo_desc_sr, 
            (select name_sr from category where category.id = product.category) as category
            FROM " . self::$table_name . "
            WHERE id={$id} LIMIT 1");
        }
        
        return!empty($result_array) ? array_shift($result_array) : false;
    }
    public static function find_by_partner($id){
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " order by updated DESC");
        return!empty($result_array) ? $result_array : false;
    }
    public static function find_by_id($id = 0) {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE id={$id} LIMIT 1");
        return!empty($result_array) ? array_shift($result_array) : false;
    }
    
    public static function find_by_category($id = 0) {
           $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE category={$id}");
           return!empty($result_array) ? $result_array : false;
       }
    public static function find_for_category($id = 0) {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE category={$id} and active = 1 order by created DESC, updated DESC");
        return!empty($result_array) ? $result_array : false;
    }


    public static function find_by_sql($sql = "") {
        global $database;
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = $database->fetch_array($result_set)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }
    public function nextPID(){
       global $database;
        $query = "select max(plD) from ".self::$table_name;
        $result_set = $database->query($query);
        $row = $database->fetch_array($result_set);
        return array_shift($row)+1;
    }

    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }
        public static function count_by_cat($id) {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name.' where category = '.$id;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }
    public static function count_by_cat_act($id) {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name.' where active =1 and category = '.$id;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }
        public static function count_active() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name.' where active =1 ';
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }


    private static function instantiate($record) {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute) {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public function save() {
        // A new record won't have an id yet.
        return isset($this->id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        if ($database->query($sql)) {
            $this->id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}='{$value}'";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= join(", ", $attribute_pairs);
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $database->query($sql);
        return ($database->affected_rows() < 2) ? true : false;
    }

    public function delete() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - DELETE FROM table WHERE condition LIMIT 1
        // - escape all values to prevent SQL injection
        // - use LIMIT 1
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $sql .= " LIMIT 1";
        $database->query($sql);
        return ($database->affected_rows() < 2) ? true : false;

        // NB: After deleting, the instance of User still
        // exists, even though the database entry does not.
        // This can be useful, as in:
        //   echo $user->first_name . " was deleted";
        // but, for example, we can't call $user->update()
        // after calling $user->delete().
    }

}
?>