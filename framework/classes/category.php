<?php

// If it's going to need the database, then it's
// probably smart to require it before we start.
require_once(LIBS_DIR . 'database.php');

class Category extends DatabaseObject {

    protected static $table_name = "category";
    protected static $db_fields = array( 'id', 'name_sr', 'name_en', 'name_ru', 'active', 'seo_title_sr', 'seo_title_en', 'seo_title_ru', 'seo_desc_sr', 'seo_desc_en', 'seo_desc_ru', 'seo_keywords_sr', 'seo_keywords_en', 'seo_keywords_ru', 'type_category' );
    
    public $id;             
    public $name_sr;
    public $name_en;       
    public $name_ru;       
    public $active;       
    public $seo_title_sr;
    public $seo_title_en;  
    public $seo_title_ru;  
    public $seo_desc_sr;  
    public $seo_desc_en;   
    public $seo_desc_ru;   
    public $seo_keywords_sr;
    public $seo_keywords_en;
    public $seo_keywords_ru;
    public $type_category;
    

    // Common Database Methods
    public static function find_all() {
        return self::find_by_sql("SELECT *, (select file from gallery where gallery.refID = category.id and  ) FROM " . self::$table_name);
    }
    public static function find_main(){
        return self::find_by_sql("SELECT * FROM " . self::$table_name." where active =1 and parent_cat= 0 ");
    }
    public static function find_children($id){
        return self::find_by_sql("SELECT * FROM " . self::$table_name." where parent_cat= ".$id);
    }
    public static function find_act_children($id){
        return self::find_by_sql("SELECT * FROM " . self::$table_name." where active =1 and name_sr!='naslovna' and parent_cat= ".$id);
    }
    public static function find_active($type=1) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name." where active =1 and name_sr!='naslovna' and type_category= '".$type."' ");
    }
    public static function  find_rand(){
        $result_array = self::find_by_sql("select * from ".self::$table_name." where type_category=2 AND name_sr!='naslovna' order by rand()");
        return array_shift($result_array);
    }
    public static function find_by_id($id = 0) {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE id={$id} LIMIT 1");
        return!empty($result_array) ? array_shift($result_array) : false;
    }
    public static function getPath($pr){
        $path = array();
        $parrent_cat = array_shift(self::find_by_sql("select id, name_en, name_sr, parent_cat from ".self::$table_name." where name_sr = '{$pr}' limit 1"));
        if($parrent_cat){
        $path[] = $parrent_cat;
        if($parrent_cat->parent_cat!=0){
           $nextCat = array_shift(self::find_by_sql("select id, name_en, name_sr, parent_cat from ".self::$table_name." where id = '{$parrent_cat->parent_cat}' limit 1"));
           if($nextCat){
               $path[] = $nextCat;
               if($nextCat->parent_cat !=0){
                   $nextNextCat = array_shift(self::find_by_sql("select id, name_en, name_sr, parent_cat from ".self::$table_name." where id = '{$nextCat->parent_cat}' limit 1"));
                   if($nextNextCat){
                       $path[]=$nextNextCat;
                       $nextNextNextCat = array_shift(self::find_by_sql("select id, name_en, name_sr, parent_cat from ".self::$table_name." where id = '{$nextNextCat->parent_cat}' limit 1"));
                        if($nextNextNextCat){
                            $path[] = $nextNextNextCat;
                            $nextNextNextNextCat =array_shift(self::find_by_sql("select id, name_en, name_sr, parent_cat from ".self::$table_name." where id = '{$nextNextNextCat->parent_cat}' limit 1"));
                            if($nextNextNextNextCat){
                                $path[] = $nextNextNextNextCat;
                            }
                        }
                   }
               }
           }
        }
        }
        return $path;
    }

    public static function find_by_sql($sql = "") {
        global $database;
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = $database->fetch_array($result_set)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }
        public static function count_active() {
        global $database;
        $sql = "SELECT COUNT(id) FROM " . self::$table_name." where active = '1'" ;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    private static function instantiate($record) {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute) {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public function save() {
        // A new record won't have an id yet.
        return (isset($this->id) && $this->id!='') ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        if ($database->query($sql)) {
            $this->id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}='{$value}'";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= join(", ", $attribute_pairs);
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $database->query($sql);
        return ($database->affected_rows() <2) ? true : false;
    }

    public function delete() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - DELETE FROM table WHERE condition LIMIT 1
        // - escape all values to prevent SQL injection
        // - use LIMIT 1
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $sql .= " LIMIT 1";
        $database->query($sql);
        return ($database->affected_rows() < 2) ? true : false;

        // NB: After deleting, the instance of User still
        // exists, even though the database entry does not.
        // This can be useful, as in:
        //   echo $user->first_name . " was deleted";
        // but, for example, we can't call $user->update()
        // after calling $user->delete().
    }

}

?>