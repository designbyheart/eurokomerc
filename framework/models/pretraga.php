<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 4/20/12
 * Time: 2:16 AM
 * To change this template use File | Settings | File Templates.
 */
$searchTerm  = NULL;
if(isset($_POST['searchF'])){
    $searchTerm = mysql_real_escape_string(trim($_POST['searchF']));
}
$searchRes = array();

if($searchTerm){
    $queryCategory = "select * from category where (name_sr LIKE '%{$searchTerm}%' OR name_en LIKE '%{$searchTerm}%' OR name_ru LIKE '%{$searchTerm}%') AND active=1";
    $queryProduct = "select * from product where (name_sr LIKE '%{$searchTerm}%' OR name_en LIKE '%{$searchTerm}%'  OR name_ru LIKE '%{$searchTerm}%') AND active=1 ";

    $categories = Category::find_by_sql($queryCategory);
    $products = Product::find_by_sql($queryProduct);
    $searchRes['categories'] = $categories;
    $searchRes['products'] = $products;
}
//print_r($searchRes);