<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 4/20/12
 * Time: 1:42 AM
 * To change this template use File | Settings | File Templates.
 */?>

<?php require_once(INC.'sidebar.php');?>
<section class="rightContent" id="outer">
    <h1><?=trans($product->name_sr, $product->name_en, $product->name_ru)?></h1>

    <article class="productDesc">
        <?=trans($product->desc_sr, $product->desc_en, $product->desc_ru)?>

    </article>

</section>