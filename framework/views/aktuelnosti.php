<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 4/21/12
 * Time: 11:33 PM
 * To change this template use File | Settings | File Templates.
 */?>
<?php require_once(INC.'sidebar.php');?>
<div class="rightContent" id="outer">
    <h1><?=trans('Aktuelnosti', 'Actualities')?></h1>
    <nav class="breadcrumb">
        <a href="<?=SITE_ROOT?>"><?=trans('Naslovna', "Home page", "")?></a> /
        <strong>Aktuelnosti</strong>
    </nav>
    <ol class="slats">
    <?php
    if($n){
    foreach($n as $n): ?>
    <article class="actialityItem">
        <span class="illustration">
            <img src="<?=SITE_ROOT?>images/gallery/vojni-program.jpg" alt=""></span>
        <h3><?=trans($n->title_sr,$n->title_en, $n->title_ru);?></h3>
        <p class="date"><?=formatDate($n->date)?></p>
        <p>
            <?=trans($n->content_sr,$n->content_en, $n->content_ru);?> asD

        </p>
        <a class="details" href="#">detaljnije</a>
    </article>

	<li class="group">
        <h3>
            <a href="#">
                <span class="illustration">
                    <img src="<?=SITE_ROOT?>images/gallery/vojni-program.jpg"/>
                </span>
                New design of Eurokomerc website
            </a>
        </h3>

        <p> <?=trans($n->content_sr, $n->content_en, $n->content_ru)?>
            <span class="meta">August 10, 2011</span></p>
    </li>
    <?php endforeach;
    } ?>

</div>