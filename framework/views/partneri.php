
<div class="path">
    <ul class="breadcrumb">
        <li><?=trans('Vi ste ovde: ', 'You are here: ')?></li>
        <li>
            <a href="<?=SITE_ROOT?>" class="homeLink">
                <img src="<?=SITE_ROOT?>assets/img/home-icon.png" class="homeIcon" alt="<?=trans('naslovna','homepage');?>">
            </a>
            <span class="separator">/</span>
        </li>
        <li>
            <p><?=trans('Partneri', 'Partners')?></p>
        </li>
    </ul>
</div>

<div class="content" id="outer">
    <div class="container">
        <div class="row">
            <div class="span9">
                <h2><?=trans('Partneri', 'Partners')?></h2>

                <div class="partners">
                    <?php foreach($partners as $p):
                    $pName = trans($p->name_sr, $p->name_en);
                    $country = '';
                    if(trans($p->country_sr, $p->country_en)!=''){
                        $country = ', <span>'.trans($p->country_sr, $p->country_en).'</span>';
                    }
                    $logo = SITE_ROOT. "images/gallery/thumbsM/defaultCat.png";
                    if($p->logo!=''){
                        $logo = SITE_ROOT. "images/gallery/thumbsM/".$p->logo;
                    }
                    ?>
                    <span class="partner">
                        <a href="<?=SITE_ROOT?>partner/<?=$p->id.'/'.urlSafe($pName)?>" class="logoLink">
                            <img src="<?=$logo?>" alt="">
                        </a>
                        <article>
                            <a href="<?=SITE_ROOT?>partner/<?=$p->id.'/'.urlSafe($pName)?>">
                                <h3><?=$pName.$country?></span></h3>
                            </a>
                            <a href="http://<?='http://'.str_replace('http://', '', $p->url)?>" class="partnerLink"><?=str_replace('http://', '', $p->url)?></a>

                            <p><?=trunc(trans($p->desc_sr, $p->desc_en), 50)?></p>
                            <a class="btn" href="<?=SITE_ROOT?>partner/<?=$p->id.'/'.urlSafe($pName)?>"><?=trans('detaljnije', 'read more')?></a>
                        </article>
                    </span>

                    <?php endforeach;?>
                </div>
            </div>
            <?php require_once(INC.'sidebar.php');?>
        </div>
    </div>
</div>