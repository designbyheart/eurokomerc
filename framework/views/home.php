<section class="slide">
    <article class="popular">
        <a class="subtitle" href="#"><?=trans('O nama', 'About us')?></a>
        <?php $pg = GeneralPage::find_by_name('o-nama');
        ?>
        <h2><?=trans('Eurokomerc kompanija', 'Eurokomerc Company', '')?></h2>

        <article><?=textOnly($pg->desc_sr, $pg->desc_en, $pg->desc_ru, true, 400)?></article>
        <a class="details" href="<?=SITE_ROOT?>o-nama"><?=trans('detaljnije', 'details')?></a>
    </article>
    <article class="slider">
        <div class="mainSlide">
            <a href="#">
                <span class="img"> <img src="<?=SITE_ROOT?>images/gallery/slide.jpg" alt=""></span>
            </a>
            <a href="#" >
                <img src="<?=SITE_ROOT?>images/gallery/slide.jpg" alt="">
            </a>
            <a href="#" >
                <img src="<?=SITE_ROOT?>images/gallery/slide.jpg" alt="">
            </a>
            <a href="#" >
                <img src="<?=SITE_ROOT?>images/gallery/slide.jpg" alt="">
            </a>
        </div>
        <div class="paging">

        </div>
    </article>
</section>
<section class="quote">
    <?php $q = Quote::getQuote();?>
    <h2><?=trans($q->headline_sr,$q->headline_en,$q->headline_ru )?></h2>
</section>
<section class="sectionTriple">
    <?php
        $pr = Product::findPopularRandom();
        if($pr){
    ?>
    <article><a class="subtitle" href="<?=SITE_ROOT?>proizvodi"><?=trans('Popularni proizvodi', 'Popular products'); ?></a>
        <h3><?=trans($pr->name_sr, $pr->name_en, $pr->name_ru)?></h3>
        <p class="galImg">
            <span><?=galleryImg($pr->id, 'product', false, 1)?></span>
        </p>
        <a class="details" href=""><?=trans('detaljnije', 'details'); ?></a>
        <?php } ?>
    </article>
    <article><a class="subtitle" href="#">
        <?=trans('PDF Download', 'Download our catalog'); ?>
    </a>
        <h3><?=trans('Katalog proizvoda', 'Products catalog'); ?></h3>

        <p  class="galImg">
            <span>
                <img src="<?=SITE_ROOT?>images/katalog.jpg" alt="Download pdf catalog" style="position: relative;top:-10px">
            </span>
        </p>
        <a class="details" href="<?=SITE_ROOT?>eurokomerc-katalog"><?=trans('download', 'download'); ?></a>
    </article>
    <article>
        <a class="subtitle" href="<?=SITE_ROOT?>galerija">

            <?=trans('Galerija proizvoda', 'Products gallery'); ?>
        </a>
        <?php $g = Category::find_rand();
            if($g){?>
        <h3><?=trans($g->name_sr, $g->name_en)?></h3>
        <p  class="galImg">
            <span>
                <?=galleryImg($g->id, 'cat-menu')?>
            </span>
        </p>
        <a class="details" href="#"><?=trans('detaljnije', 'details'); ?></a>
                <?php } ?>
    </article>
</section>