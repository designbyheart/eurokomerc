<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 4/19/12
 * Time: 3:39 PM
 * To change this template use File | Settings | File Templates.
 */
require_once INC.'sidebar.php';
?>

<div class="rightContent">
    <h1>Kontaktirajte nas</h1>
    <nav class="breadcrumb">
        <a href="<?=SITE_ROOT?>"><?=trans('Naslovna', "Home page", "")?></a> /
        <strong>Kontakt</strong>
    </nav>
    <section>
        <article class="halfWidth"><?=trans($gP->desc_sr, $gP->desc_en)?></article>

        <article class="halfWidth">
            <? require_once INC.'kontaktSidebar.php';?>
        </article>
    </section>

    <div class="mapContainer">
        <div class="map"></div>
    </div>
</div>