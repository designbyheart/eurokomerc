<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 4/20/12
 * Time: 1:08 AM
 * To change this template use File | Settings | File Templates.
 */
 ?>
<?php require_once(INC.'sidebar.php');?>
<div class="rightContent" id="outer">
                <h1>Reference</h1>
    <nav class="breadcrumb">
        <a href="<?=SITE_ROOT?>"><?=trans('Naslovna', "Home page", "")?></a> /

        <strong>Reference</strong>
    </nav>
    <ol class="slats">

                <?php foreach($reference as $ref):
                    if($ref->url==''){
                        $ref->url = '#';
                    }else{
                        $ref->url = 'http://'.$ref->url;
                    }
                ?>

        <li class="group">
            <h3><a href="<?=$ref->url?>" target="_blank">
                <?=galleryImg($ref->id, 'reference')?>
                <?=trans($ref->headline_sr, $ref->headline_en)?></a></h3>
            <article><?=trans($ref->subline_sr, $ref->subline_en)?></article>
        </li>

    <?php endforeach;?>
    </ol>

</div>