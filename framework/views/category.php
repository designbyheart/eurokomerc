<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 4/20/12
 * Time: 12:28 AM
 * To change this template use File | Settings | File Templates.
 */?>

<div class="path">
    <ul class="breadcrumb">
        <li>Vi ste ovde: </li>
        <li>
            <a href="#" class="homeLink"> <img src="<?=SITE_ROOT?>assets/img/home-icon.png" class="homeicon" alt="<?=trans('naslovna','homepage');?>"></a> <span class="divider">/</span>
        </li>
        <li><a href="<?=SITE_ROOT?>proizvodi"><?=trans('Proizvodi', 'Products')?></a>
            <span class="divider">/</span>
        </li>
        <?=breadCrumb(Category::getPath($cat->name_sr))?>
    </ul>
</div>

<div class="content" id="outer">
    <div class="container">
        <div class="row">
            <div class="span9">
                <h2><?=trans($cat->name_sr, $cat->name_en)?></h2>
                <div class="proizvodi">
                    <?php
                    if(!$_isSubCats){
//                        there is no subcategories, show products only
                        if(count($catProduct)>0 && $catProduct!=''){
                        foreach($catProduct as $pr):
                            $img = SITE_ROOT.'images/gallery/thumbsM/defaultCat.png';
                            $gallery = getGallery($pr->id, 'product');
                            if($gallery){
                                $img = SITE_ROOT.'images/gallery/'.$gallery;
                            }
                        ?>
                        <div class="box">
                            <span class="title"><a href="<?=SITE_ROOT?>proizvod/<?=$pr->id?>/<?=urlSafe(trans($pr->name_sr,$pr->name_en))?>"><?=trans($pr->name_sr, $pr->name_en)?></a></span>
                            <a href="<?=SITE_ROOT?>proizvod/<?=$pr->id?>/<?=urlSafe(trans($pr->name_sr,$pr->name_en))?>" class="img-link">
                                <span>
                                    <img src="<?=$img?>" alt=""<?=trans($pr->name_sr, $pr->name_en)?> />
                                </span>
                            </a>
                            <span class="sifra"><?=trans('Šifra', 'Code')?>: <?=$pr->plD?></span>
                            <?php //TODO :: konekcija sa shopcartom ?>
                            <span class="korpa" style="display:none">
                                <a href="#"><?=trans('poruči','order');?> 
                                    <span class="icon-shopping-cart"></span>
                                </a>
                            </span>
                        </div>
                        <?php endforeach;
                        }else{ ?>
                            <p>Trenutno nema proizvoda u ovoj kategoriji.</p>
                            <?php
                        }
                    }else{
                        //show subcategories
                        foreach($subCats as $cat):
                            $img = SITE_ROOT.'images/gallery/thumbsM/defaultCat.png';
                            $gallery = getGallery($cat->id, 'cat');
                            if($gallery){
                                $img = SITE_ROOT.'images/gallery/'.$gallery;
                            }

                            ?>
                                <div class="box">
                                    <a href="<?=SITE_ROOT?>kategorija/<?=$cat->id?>/<?=urlSafe(trans($cat->name_sr,$cat->name_en))?>" class="img-link">
                                        <span>
                                            <img src="<?=$img?>" alt="<?=trans($cat->name_sr, $cat->name_en)?>" />
                                        </span>
                                    </a>
                                    <span class="title"><a href="<?=SITE_ROOT?>kategorija/<?=$cat->id?>/<?=urlSafe(trans($cat->name_sr,$cat->name_en))?>"><?=trans($cat->name_sr,$cat->name_en);?></a></span>
                                </div>
                        <?php endforeach;
                    }?>
                </div>
            </div>
            <?php require_once(INC.'sidebar.php');?>
        </div>
    </div>
</div>