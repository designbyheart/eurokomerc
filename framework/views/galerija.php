<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 5/29/12
 * Time: 1:55 AM
 * To change this template use File | Settings | File Templates.
 */
    require_once INC.'sidebarCategories.php';
?>

<div class="rightContent">
    <h1><?=trans('Galerija proizvoda i usluga', 'Product gallery')?></h1>

    <nav class="breadcrumb">
        <a href="<?=SITE_ROOT?>"><?=trans('Naslovna', "Home page", "")?></a> /
        <strong>Galerija proizvoda i usluga</strong>
    </nav>

    <?php foreach($galery as $g): ?>
    <article class="galleryItem">
        <a href="<?=SITE_ROOT?>galerija/<?=$g->id?>/<?=urlSafe(trans($g->name_sr, $g->name_en))?>">
            <?=galleryImg($g->id, 'cat-menu')?>
            <span><?=trans($g->name_sr, $g->name_en, $g->name_ru)?></span>
        </a>
    </article>
    <?php endforeach; ?>
</div>
