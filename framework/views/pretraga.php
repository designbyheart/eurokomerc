
<!---->
<!--<div class="content" id="outer">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="span9">-->
<!--                <h2>--><?php //echo trans("Rezultati pretrage: ", "Search results: ");
//                    if(isset($_POST['search'])){
//                        echo $_POST['search'];
//                    }
//                    ?><!--</h2>-->
<!--                --><?php
//                $term = $_POST['search'];
//                if(strlen(trim($term))>2){
//                    $result = (makeSearch($term));
//                    if(count($result)<1){
//                        echo '<p>Za traženi pojam nema rezultata.</p>';
//                    }   else{
//                        echo count($result[0]['products']);
//                    }
//                }else{
//                    $error = "GREŠKA:<br> Molimo unesite pojam za pretragu koji sadrži više od tri karaktera.";
//                } ?>
<!--                --><?php //if(isset($error)){ echo $error; }?>
<!--            </div>-->
<!--            --><?php //require_once(INC.'sidebar.php');?>
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 4/20/12
 * Time: 1:08 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<?php require_once(INC.'sidebar.php');?>
<div class="rightContent" id="outer">
    <h1><span class="small"><?=trans('Rezultati pretrage: ', "Search results: ").'</span>'.$_POST['searchF']?></h1>
    <ol class="slats">
        <?php
        if(isset($searchRes['categories']) && count($searchRes['categories'])>0){
            ?>
            <h4>Kategorije proizvoda (<?=count($searchRes['categories'])?>)</h4>
            <?php
            foreach($searchRes['categories'] as $cat):
                $catName = trans($cat->name_sr, $cat->name_en, $cat->name_ru);
                $type = 'kategorija';
                if($cat->type_category==2){
                    $type = 'galerija';
                }
                ?>
                <li class="group">
                    <h3>
                        <a href="<?=SITE_ROOT.$type.'/'.$cat->id.'/'.urlSafe($catName)?>">
                            <?=galleryImg($cat->id, 'cat-menu', true, 1);?>
                            <?=$catName?>
                        </a>
                    </h3>
                </li>
            <?php endforeach;
        }
        if(isset($searchRes['products']) && count($searchRes['products'])>0){
            ?>
            <h4>Proizvodi (<?=count($searchRes['products'])?>)</h4>
            <?php
            foreach($searchRes['products'] as $cat):
                $catName = trans($cat->name_sr, $cat->name_en, $cat->name_ru);
                ?>
                <li class="group">
                    <h3>
                        <a href="<?=SITE_ROOT?>proizvod/<?=$cat->id.'/'.urlSafe($catName)?>">
                            <?=galleryImg($cat->id, 'product', true, 1);?>
                            <?=$catName?>
                        </a>
                    </h3>
                    <p>
                        <?=strip_tags(trunc(trans($cat->desc_sr, $cat->desc_en, $cat->desc_ru), 200))?>
                    </p>
                </li>
                <?php endforeach;
        }

        ?>
    </ol>

</div>