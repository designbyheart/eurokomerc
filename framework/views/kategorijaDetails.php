<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 6/1/12
 * Time: 2:28 AM
 * To change this template use File | Settings | File Templates.
 */
require_once INC.'sidebarCategories.php';
?>
<div class="rightContent">
    <h1><?=trans($category->name_sr, $category->name_en)?></h1>
    <section class="galleryProducts galeryDetails">
        <?php if($products){ foreach($products as $pr):
        $file = array_shift(Gallery::find($pr->id, 'product'));
        if($file && file_exists(ROOT_DIR.'images/gallery/thumbsM/'.$file->file)){
            ?>
            <article class="galleryItem">
                <a href="<?=SITE_ROOT?>proizvod/<?=$pr->id?>/<?=urlSafe(trans($pr->name_sr, $pr->name_en))?>">
                    <span class="imgContainer"><?=galleryImg($pr->id, 'product')?></span>
                    <span class="productTitle"><?=trans($pr->name_sr, $pr->name_en, $pr->name_ru)?></span>
                </a>
            </article>
            <?php }
    endforeach;
    } else{ ?>
        <p><?=trans("Za izabranu kategoriju trenutno nema proizvoda", "There is no products available for selected group")?></p>
    <?php }
        ?>
    </section>
</div>