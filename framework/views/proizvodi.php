<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 5/29/12
 * Time: 1:55 AM
 * To change this template use File | Settings | File Templates.
 */
require_once INC.'sidebarCategories.php';
?>

<div class="rightContent">
    <h1><?=trans('Proizvodi', 'Products')?></h1>
    <nav class="breadcrumb">
        <a href="<?=SITE_ROOT?>"><?=trans('Naslovna', "Home page", "")?></a> /
        <strong><?=trans('Proizvodi', 'Products')?></strong>
    </nav>
    <?php foreach($gallery as $pr): ?>
    <article class="galleryItem">
        <a href="<?=SITE_ROOT?>kategorija/<?=$pr->id?>/<?=urlSafe(trans($pr->name_sr, $pr->name_en))?>">
            <span class="imgContainer"><?=galleryImg($pr->id, 'cat-menu')?>   </span>
            <span><?=trans($pr->name_sr, $pr->name_en, $pr->name_ru)?></span>
        </a>
    </article>
    <?php endforeach; ?>
</div>
