<?php require_once(INC . "sidebar.php"); ?>
<div class="rightContent">


    <h1><?=trans($gP->name_sr, $gP->name_en)?></h1>

    <nav class="breadcrumb">
        <a href="<?=SITE_ROOT?>"><?=trans('Naslovna', "Home page", "")?></a> /
        <strong>O nama</strong>
    </nav>
    <article>
        <?=trans($gP->desc_sr, $gP->desc_en)?>

    </article>

    <div class="gallery">
        <a href="#"><img src="" alt="">
        </a>
        <a href="#">
            <img src="" alt="">
        </a>
        <a href="#">
            <img src="" alt="">
        </a>
    </div>
</div>