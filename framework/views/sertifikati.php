<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 5/29/12
 * Time: 1:54 AM
 * To change this template use File | Settings | File Templates.
 */      ?>
<?php require_once(INC.'sidebar.php');?>
<div class="rightContent" id="outer">
    <h1>Sertifikati</h1>
    <nav class="breadcrumb">
        <a href="<?=SITE_ROOT?>"><?=trans('Naslovna', "Home page", "")?></a> /
        <strong>Sertifikati</strong>
    </nav>

    <?=trans($g->desc_sr,$g->desc_en,$g->desc_ru)?>
    <ol class="slats">

        <?php foreach($certificates as $cert):
        ?>

        <li class="group">
            <h3><a href="<?=SITE_ROOT.'sertifikat/'.$cert->id.'/'.urlSafe(trans($cert->headline_sr, $cert->headline_en, $cert->headline_ru))?>" target="_blank">
                <?=galleryImg($cert->id, 'certificate')?>
                <?=trans($cert->headline_sr, $cert->headline_en)?></a></h3>
            <article><?=trans($cert->subline_sr, $cert->subline_en)?></article>
        </li>

        <?php endforeach;?>
    </ol>
</div>