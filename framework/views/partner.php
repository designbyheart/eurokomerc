<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 5/1/12
 * Time: 12:33 AM
 * To change this template use File | Settings | File Templates.
 */

?>
<div class="path">
    <ul class="breadcrumb">
        <li>
        <li> Vi ste ovde:</li>
        <li><a href="<?=SITE_ROOT?>" class="homeLink">
            <img src="<?=SITE_ROOT?>assets/img/home-icon.png" class="homeicon" alt="<?=trans('naslovna','homepage');?>">
        </a>
            <span class="separator">/</span>
        </li>
        <li><a href="<?=SITE_ROOT?>partneri"><?=trans('Partneri', 'Partners ')?></a> <span class="separator">/</span></li>
        <li>
            <p><?=trans($partner->name_sr, $partner->name_en)?></p>
        </li>
    </ul>
</div>
<div class="content" id="outer">
    <div class="container">
        <div class="row">
            <div class="span9">
                <?php //check is page exists or is active     ?>

                <h2><?=trans($partner->name_sr, $partner->name_en);?></h2>

                <div class="partnerDescription">
                    <a href="<?='http://'.str_replace('http://', '', $partner->url)?>" target="_blank" class="partnerLogo">
                        <img src="<?=SITE_ROOT?>images/gallery/<?=$partner->logo?>" alt="">
                    </a>
                    <table>
                        <tr>
                            <td>
                                <p><?=trans('Zemlja', 'Country')?></p>
                            </td>
                            <td>
                                <p class="partnerInfo"><?=trans($partner->country_sr, $partner->country_en)?></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>web:</p>
                            </td>
                            <td>
                                <p class="partnerInfo"><a href="<?='http://'.str_replace('http://', '', $partner->url)?>" target="_blank"><?=$partner->url?></a></p>
                            </td>
                        </tr>

                    </table>
                </div>

                <h2>Opis</h2>
                <section class="partnerDesc">

                    <?=trans($partner->desc_sr, $partner->desc_en)?>
                </section>

                <?php if($products){?>
                <h2>Proizvodi ovog proizvodjača</h2>
                <div class="proizvodi">
                    <?php foreach($products as $pr):
                    $img = SITE_ROOT.'images/gallery/thumbsM/defaultCat.png';
                    $gallery = getGallery($pr->id, 'product');
                    if($gallery){
                        $img = SITE_ROOT.'images/gallery/'.$gallery;
                    }
                    ?>
                    <div class="box">
                        <span class="title">
                            <a href="<?=SITE_ROOT?>proizvod/<?=$pr->id?>/<?=urlSafe(trans($pr->name_sr,$pr->name_en))?>">
                                <?=trans($pr->name_sr,$pr->name_en);?>
                            </a>
                        </span>
                        <a href="<?=SITE_ROOT?>proizvod/<?=$pr->id?>/<?=urlSafe(trans($pr->name_sr,$pr->name_en))?>" class="img-link">
                            <span>
                                <img src="<?=$img?>" alt="<?=trans($pr->name_sr,$pr->name_en)?>" />
                            </span>
                        </a>
                        <span class="sifra">Šifra: <?=$pr->plD?></span>
                        <?php //TODO:: dodati kasnije poruci link ?>
                        <span class="korpa" style="display:none"><a href="#">poruči <span class="icon-shopping-cart"></span></a></span>
                    </div>
                    <?php  endforeach; ?>

                </div>
                <?php } ?>
            </div>
            <?php require_once(INC.'sidebar.php');?>
        </div>
    </div>
</div>