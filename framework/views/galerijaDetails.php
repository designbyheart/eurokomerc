<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 6/1/12
 * Time: 1:58 AM
 * To change this template use File | Settings | File Templates.
 */
require_once INC.'sidebarCategories.php';
?>
<div class="rightContent">
    <h1><?=trans($category->name_sr, $category->name_en)?></h1>
    <nav class="breadcrumb">
        <a href="<?=SITE_ROOT?>"><?=trans('Naslovna', "Home page", "")?></a> /
        <a href="<?=SITE_ROOT?>galerija"><?=trans('Galerija', "Gallery", "")?></a> /
        <strong><?=trans($category->name_sr, $category->name_en)?></strong>
    </nav>
        <section class="galleryProducts galeryDetails">
        <?php foreach($gallery as $gl):
        if(file_exists(ROOT_DIR.'images/gallery/thumbsM/'.$gl->file)){
            ?>
            <article class="galleryItem">
                <a href="#">
                    <img src="<?=SITE_ROOT?>images/gallery/thumbsM/<?=$gl->file?>" alt="">
                </a>
            </article>
            <?php }
    endforeach; ?>
    </section>
</div>