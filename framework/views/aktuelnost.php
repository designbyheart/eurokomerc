<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 4/21/12
 * Time: 11:34 PM
 * To change this template use File | Settings | File Templates.
 */?>
<div class="path">
    <ul class="breadcrumb">
        <li><?=trans("Vi ste ovde", "You are here")?></li>
        <li>
            <a href="#" class="homeLink"> <img src="<?=SITE_ROOT?>assets/img/home-icon.png" class="homeicon" alt="<?=trans('naslovna','homepage');?>"></a> <span class="divider">/</span>
        </li>
        <li><a href="<?=SITE_ROOT?>aktuelnosti"><?=trans('Aktuelnosti', 'Actualities')?></a>
            <span class="divider">/</span>
        </li>
        <li>
            <b><?=trans($n->title, $n->title_en)?></b></li>
        </li>
    </ul>
</div>
<div class="content" id="outer">
    <div class="container">
        <div class="row">
            <div class="span9">
                <h2><?=trans($n->title, $n->title_en)?></h2>

                <p class="date"><?=formatDate($n->date)?></p>
                <span>
                    <?=trans($n->content,$n->content_en);?>
                </span>

            </div>
            <?php require_once(INC.'sidebar.php');?>
        </div>
    </div>
</div>