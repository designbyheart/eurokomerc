<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 6/3/12
 * Time: 2:44 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('framework/lib/setup.php');
$product = Product::find_by_sql("-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 03, 2012 at 02:43 PM
-- Server version: 5.5.9
-- PHP Version: 5.3.6

SET SQL_MODE=\"NO_AUTO_VALUE_ON_ZERO\";

--
-- Database: `eurokomerc`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `e_mail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `tel` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `role` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` VALUES(1, 'pedja', '1858b78054f5328781ab9c1b3d30b6d3', 'Pedja', 'Jevtic', 'designbyheart@gmail.com', '+381 60 707.707.9', 'administrator');
INSERT INTO `administrator` VALUES(4, 'branka', 'f698ead3a9a6e03610f21a8ee27d8af1', 'Branka', 'Stanic', 'brankastanic86@gmail.com', '060300422', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(256) NOT NULL,
  `name_en` varchar(256) NOT NULL,
  `name_ru` varchar(256) NOT NULL,
  `active` int(11) NOT NULL,
  `seo_title_sr` varchar(256) NOT NULL,
  `seo_title_en` varchar(256) NOT NULL,
  `seo_title_ru` varchar(256) NOT NULL,
  `seo_desc_sr` text NOT NULL,
  `seo_desc_en` text NOT NULL,
  `seo_desc_ru` text NOT NULL,
  `seo_keywords_sr` text NOT NULL,
  `seo_keywords_en` text NOT NULL,
  `seo_keywords_ru` text NOT NULL,
  `type_category` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` VALUES(1, 'tenk1', 'tenk en', 'tenk ru', 0, 'tenk', '', '', 'tenk', '', '', '', '', '', 0);
INSERT INTO `category` VALUES(2, 'tenk2', '', '', 0, '', '', '', '', '', '', '', '', '', 0);
INSERT INTO `category` VALUES(3, 'tenk3', '', '', 0, '', '', '', '', '', '', '', '', '', 0);
INSERT INTO `category` VALUES(4, 'tenk4', '', '', 0, '', '', '', '', '', '', '', '', '', 0);
INSERT INTO `category` VALUES(6, 'Vojni program', 'MIlitary program', 'Ð²Ð¾ÐµÐ½Ð½Ñ‹Ðµ Ð¿Ñ€Ð¾Ð³Ñ€Ð°Ð¼Ð¼Ñ‹', 1, '', '', '', '', '', '', '', '', '', 2);
INSERT INTO `category` VALUES(7, 'OznaÄavanje crevovoda', '', '', 1, '', '', '', '', '', '', '', '', '', 1);
INSERT INTO `category` VALUES(8, 'Fleksibilna creva', 'Flexible hoses', '', 1, '', '', '', '', '', '', '', '', '', 1);
INSERT INTO `category` VALUES(9, 'ÄŒaure za creva', 'Ferrules for hoses', '', 1, '', '', '', '', '', '', '', '', '', 1);
INSERT INTO `category` VALUES(10, 'PRIKLJUÄŒCI ZA CREVA', '', '', 0, '', '', '', '', '', '', '', '', '', 0);
INSERT INTO `category` VALUES(11, 'naslovna', '', '', 1, '', '', '', '', '', '', '', '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `certificate`
--

CREATE TABLE `certificate` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `headline_sr` text,
  `headline_en` text,
  `headline_ru` text,
  `subline_sr` text,
  `subline_en` text,
  `subline_ru` text,
  `active` int(1) NOT NULL,
  `file` varchar(128) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `certificate`
--


-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `status` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `phone` varchar(256) NOT NULL,
  `username` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `pib` varchar(256) NOT NULL,
  `date` datetime NOT NULL,
  `reg_number` int(11) NOT NULL,
  `city` varchar(256) NOT NULL,
  `notification` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `client`
--


-- --------------------------------------------------------

--
-- Table structure for table `client_log`
--

CREATE TABLE `client_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `log_desc` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `client_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(256) NOT NULL,
  `refID` int(11) NOT NULL,
  `type` varchar(256) NOT NULL,
  `file` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` VALUES(1, '', 0, '', '');
INSERT INTO `gallery` VALUES(3, '', 6, 'cat-menu', 'cat-menu-planetary-sunrise-with-awesome-space-view.jpg');
INSERT INTO `gallery` VALUES(4, '', 6, 'cat', 'cat-the-moon.jpg');
INSERT INTO `gallery` VALUES(6, '', 42, 'cat-menu', 'cat-menu-high_tatras2.jpg');
INSERT INTO `gallery` VALUES(7, '', 181, 'product', 'product-high_tatras4.jpg');
INSERT INTO `gallery` VALUES(15, '', 17, 'cat-menu', 'cat-menu-flipboard_news-02.png');
INSERT INTO `gallery` VALUES(16, '', 38, 'cat-menu', 'cat-menu-flipboard_news-01.png');
INSERT INTO `gallery` VALUES(17, '', 2, 'general_page', 'general_page-eurokomerc.png');
INSERT INTO `gallery` VALUES(18, '', 8, 'cat-menu', 'cat-menu-fimar-logo.png');
INSERT INTO `gallery` VALUES(23, '', 8, 'cat-menu', 'cat-menu-prikaz-proizvoda.jpg');
INSERT INTO `gallery` VALUES(24, '', 6, 'cat-menu', 'cat-menu-d200809020243.jpg');
INSERT INTO `gallery` VALUES(25, '', 6, 'category', 'category-32770_45573191_lazar_3-2.jpg');
INSERT INTO `gallery` VALUES(26, '', 6, 'kategorija', 'category-112015_76899775_lazar_1.jpg');
INSERT INTO `gallery` VALUES(27, '', 6, 'category', 'category-5980.jpg');
INSERT INTO `gallery` VALUES(29, '', 4, 'product', 'product-logosmall.png');
INSERT INTO `gallery` VALUES(39, '', 1, 'reference', 'reference-ministarstvo odbrane.gif');
INSERT INTO `gallery` VALUES(40, '', 7, 'cat-menu', 'cat-menu-vojska srbije.gif');
INSERT INTO `gallery` VALUES(41, '', 9, 'cat-menu', 'cat-menu-jp zelenilo beograd.png');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_copy`
--

CREATE TABLE `gallery_copy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(256) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `type` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `gallery_copy`
--

INSERT INTO `gallery_copy` VALUES(1, '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `general_page`
--

CREATE TABLE `general_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(256) NOT NULL,
  `name_en` varchar(256) NOT NULL,
  `name_ru` varchar(256) NOT NULL,
  `active` int(11) NOT NULL,
  `desc_sr` longtext NOT NULL,
  `desc_en` longtext NOT NULL,
  `desc_ru` longtext NOT NULL,
  `seo_title_sr` varchar(256) NOT NULL,
  `seo_title_en` varchar(256) NOT NULL,
  `seo_title_ru` varchar(256) NOT NULL,
  `seo_desc_sr` text NOT NULL,
  `seo_desc_en` text NOT NULL,
  `seo_desc_ru` text NOT NULL,
  `seo_keywords_sr` text NOT NULL,
  `seo_keywords_en` text NOT NULL,
  `seo_keywords_ru` text NOT NULL,
  `pg_name` varchar(128) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `general_page`
--

INSERT INTO `general_page` VALUES(1, 'Kontakt', '', '', 1, '<p><br><strong>PreduzeÄ‡e Eurokomerc d.o.o.</strong> <br>\r\n        Lole Ribara 2, 37220 Brus   <br>\r\n        tel. 037 751 080 fax. 037 752 211 <br>\r\n        e-mail: office@eurokomerc<br>\r\n        <br><br>\r\n        MatiÄni broj:\r\n        <strong>07363559 </strong>    <br>\r\n        registarski broj:\r\n            <strong>0297363559</strong>       <br>\r\n        Å¡ifra delatnosti:\r\n                <strong>51190 </strong>               <br>\r\n        pib:\r\n                    <strong>101141356</strong>                <br>\r\n        tekuÄ‡i raÄun:\r\n                        <strong>205-20881-10</strong>\r\n\r\n        <strong>160-37886-05</strong>  </p>', '<div class=\"oneone\"><p>                    				                    			</p></div>', '<div class=\"oneone\"><p> </p></div>', '', '', '', '', '', '', '', '', '', 'kontakt');
INSERT INTO `general_page` VALUES(2, 'O nama', 'About Us', '', 1, '<div class=\"oneone\">                                    <p style=\"font-weight: normal; \">\r\n		PreduzeÄ‡e Eurokomerc d.o.o. 37220 Brus, Lole Ribara 2 osnovano Je 20.10.1989.\r\n	</p>\r\n	<p> <b>Osnovna delatnost preduzeÄ‡a je:</b>\r\n		<br>\r\n		projektovanje I Proizvodnja Penumatskih I HidrauliÄnih Cevovoda, Crevovoda I Armature Za Cevi I Creva.\r\n	</p>\r\n	<p> <b>PreduzeÄ‡e Eurokomerc D.o.o. SaraÄ‘uje Sa:\r\n		</b><br>\r\n		- Vti - Vojno TehniÄki Institut, Beograd\r\n		<br>\r\n		- Institut Kirilo SaviÄ‡. Beograd\r\n		<br>\r\n		- Institut ImÅ¡, Beograd\r\n		<br>\r\n		- MaÅ¡inski Fakultet, NiÅ¡\r\n		<br>- Fon, Beograd - Katedra Za Upravljanje Kvalitetom</p>\r\n	<p>\r\n		<b>Korisnici preduzeÄ‡a Eurokomerc d.o.o. su:\r\n		</b><br>\r\n		- Jp Elektroprivreda Srbije\r\n		<br>\r\n		- Nis, Beograd\r\n		<br>\r\n		- Jp Å½eleznice Srbije, Beograd\r\n		<br>\r\n		- Vojska Srbije\r\n		<br>\r\n		- Jp Gsp, Beograd\r\n		<br>\r\n		- Jp Gsp, Novi Sad\r\n		<br>\r\n		- 14.oktobar, KruÅ¡evac\r\n		<br>\r\n		- Imt, Beograd\r\n		<br>\r\n		- Imr, Beograd\r\n		<br>\r\n		- Ikarbus, Beograd\r\n		<br>\r\n		- Zastava Automobili, Kragujevac\r\n		<br>\r\n		- Zastava Kamioni, Kragujevac\r\n		<br>\r\n		- GoÅ¡a, Smederevska Palanka\r\n		<br>\r\n		- Henkel Merima, KruÅ¡evac\r\n		<br>- Magnohrom, Kraljevo</p>                                </div>', '<div class=\"oneone\">                    				<div class=\"oneone\">                    				<div class=\"oneone\">                    				<div class=\"oneone\"><p>                    				                    			</p></div>                    			</div>                    			</div>                    			</div>', '<div class=\"oneone\"><div class=\"oneone\"><div class=\"oneone\"><div class=\"oneone\"><p> </p></div> </div> </div> </div>', '', '', '', '', '', '', '', '', '', 'o-nama');

-- --------------------------------------------------------

--
-- Table structure for table `mail`
--

CREATE TABLE `mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) NOT NULL,
  `reftitle` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `date` date NOT NULL,
  `phone` varchar(128) NOT NULL,
  `status` int(11) NOT NULL,
  `messageContent` text NOT NULL,
  `refMessageContent` text NOT NULL,
  `receiver` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mail`
--

INSERT INTO `mail` VALUES(1, 'test naslov', 'RE:[test naslov]', 'brankastanic86@gmail.com', 'Branka Stanic', '2012-04-28', '', 1, 'content', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE `manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(256) NOT NULL,
  `name_en` varchar(256) NOT NULL,
  `logo` varchar(256) NOT NULL,
  `country_sr` varchar(256) NOT NULL,
  `country_en` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `type` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `desc_sr` longtext NOT NULL,
  `desc_en` longtext NOT NULL,
  `seo_title_sr` varchar(256) NOT NULL,
  `seo_title_en` varchar(256) NOT NULL,
  `seo_desc_sr` text NOT NULL,
  `seo_desc_en` text NOT NULL,
  `seo_keywords_sr` text NOT NULL,
  `seo_keywords_en` text NOT NULL,
  `banner` int(1) NOT NULL,
  `banner_home` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `manufacturer`
--


-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_en` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title_ru` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title_sr` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `content_sr` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `content_en` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `content_ru` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date` datetime NOT NULL,
  `ordering` int(11) NOT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` VALUES(1, 'test', 0x732064666173646620617364666173646620617366, '2012-04-18 23:20:51', 1, 1, 'aafsd fasdf asd', 'f asdf asdf sa');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(256) NOT NULL,
  `name_en` varchar(256) NOT NULL,
  `name_ru` varchar(256) NOT NULL,
  `category` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `desc_sr` longtext NOT NULL,
  `desc_en` longtext NOT NULL,
  `desc_ru` longtext NOT NULL,
  `seo_title_sr` varchar(256) NOT NULL,
  `seo_title_en` varchar(256) NOT NULL,
  `seo_title_ru` varchar(256) NOT NULL,
  `seo_desc_sr` text NOT NULL,
  `seo_desc_en` text NOT NULL,
  `seo_desc_ru` text NOT NULL,
  `seo_keywords_sr` text NOT NULL,
  `seo_keywords_en` text NOT NULL,
  `seo_keywords_ru` text NOT NULL,
  `updated` int(15) NOT NULL,
  `created` int(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` VALUES(4, 'Hidraulika - Visoki pritisak', 'Hidraulic - High Pressure', '', 8, 1, 0, 'asd sfdsfsad\r\nf\r\nasdf\r\n asd\r\nf sad\r\nf\r\nasdf\r\nas\r\ndf asdf', '', '', '', '', '', '', '', '', '', '', '', 1338691188, 1338506535);

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE `quotes` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `headline_sr` text NOT NULL,
  `headline_en` text NOT NULL,
  `headline_ru` text NOT NULL,
  `subline_sr` text NOT NULL,
  `subline_en` text NOT NULL,
  `subline_ru` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` VALUES(1, 'sadf sdfasd asdf', '', '', '', '', '');
INSERT INTO `quotes` VALUES(2, 'aloha... testing first...moosofsad fasdf sdf ', 'sadf asdf adsf asdf', 'russiona', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `reference`
--

CREATE TABLE `reference` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `headline_sr` text,
  `headline_en` text,
  `headline_ru` text,
  `subline_sr` text,
  `subline_en` text,
  `subline_ru` text,
  `active` int(1) NOT NULL,
  `file` varchar(128) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `reference`
--

INSERT INTO `reference` VALUES(1, 's fasdfasdfas df', '', '', 'fadsf asdf asd', '', '', 1, '', 'asdfasd asfa sdfas');
");
