$(document).ready(function(){
	
	//sakrivanje pod menija
	$('nav ul li ul').css('display', 'none');
	
	//animacija
	$('nav ul li').hover(function(){
		$(this).children('ul')
			.css('display', 'block')
			.find('ul').css('display', 'none');
	}, function(){
		$(this).find('ul').css('display', 'none');
	});
});