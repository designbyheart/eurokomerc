$(document).ready(function(){
	$('.kategorije').jcarousel({
		wrap: 'circular'
	});
    $("#sendMessage").live('click',function(){
        var inputs = $('form.contact input');
        var error ;
        $.each(inputs, function(index, obj){
            if(obj.name ==='name' || obj.name==='email'){
                if(obj.value===''){
                    switch(obj.name){
                        case 'name':
                             $('.reqname').show();
                             error += obj.name;
                        break;
                        case 'email':
                            $('.reqemail').show();
                            error+=obj.name;
                        break;
                    }
                }else{
                    switch(obj.name){
                        case 'name':
                            $('.reqname').hide();
                            error-=obj.name;
                            break;
                        case 'email':
                            $('.reqemail').hide();
                            error-=obj.name;
                            break;
                    }
                }
            }
        });
        if($("form.contact textarea").val()==''){
            $('.reqmessage').show();
            error+='message';
        }else{
            $('.reqmessage').hide();
            error-='message';
        }
        //alert($("form.contact textarea").val());
        if($("form textarea").val()!='' && $('.contact .name').val()!='' && $('.contact .email').val()!=''){
            $('form.contact').attr('action', 'http://localhost/omnipromet/sendMessage');
            $('form.contact').submit();
        }
        return false;
    });
    $('.jcarousel-item-horizontal a').hover(function(){
        $(this).find('p').fadeOut('fast');
    }, function(){
        $(this).find('p').fadeIn('fast');
    });

    var mrakwidth = (($(document).width())-960)/2;
    $('body').prepend('<div class="levimrak"></div><div class="desnimrak"></div>');
    var pg = $('.page').val();
    $('.levimrak').css({
        'width' : mrakwidth,
        'height' : pg,
        'position' : 'absolute',
        'top' : '117px',
        'left' : '0',
        'background' : 'rgba(0, 0, 0, 0.5)',
        'z-index' : '100'
    });
    $('.desnimrak').css({
        'width' : mrakwidth,
        'height' : pg,
        'position' : 'absolute',
        'top' : '117px',
        'right' : '0',
        'background' : 'rgba(0, 0, 0, 0.5)',
        'z-index' : '100'
    });
});
