/**
 * Created with JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 4/29/12
 * Time: 11:35 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function() {
    /*
     *   Examples - images
     */

    $("a.example1").fancybox();

    $("a#example2").fancybox({
    'overlayShow'	: false,
    'transitionIn'	: 'elastic',
    'transitionOut'	: 'elastic'
    });

$("a#example3").fancybox({
    'transitionIn'	: 'none',
    'transitionOut'	: 'none'
    });

$("a#example4").fancybox({
    'opacity'		: true,
    'overlayShow'	: false,
    'transitionIn'	: 'elastic',
    'transitionOut'	: 'none'
    });

$("a#example5").fancybox();

$("a#example6").fancybox({
    'titlePosition'		: 'outside',
    'overlayColor'		: '#000',
    'overlayOpacity'	: 0.9
    });

$("a#example7").fancybox({
    'titlePosition'	: 'inside'
    });
$("a[rel=fancybox]").fancybox({
    'transitionIn'		: 'elastic',
    'transitionOut'		: 'elastic',
    'overlayColor'		: '#000',
    'titlePosition' 	: 'over',
    'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
        if(currentArray.length>1){
    return '<span id="fancybox-title-over">' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
        }else{
            return '';
        }
    }
});

/*
*   Examples - various
*/

$("#various1").fancybox({
    'titlePosition'		: 'inside',
    'transitionIn'		: 'none',
    'transitionOut'		: 'none'
    });

$("#various2").fancybox();

$("#various3").fancybox({
    'width'				: '75%',
    'height'			: '75%',
    'autoScale'			: false,
    'transitionIn'		: 'none',
    'transitionOut'		: 'none',
    'type'				: 'iframe'
    });

$("#various4").fancybox({
    'padding'			: 0,
    'autoScale'			: false,
    'transitionIn'		: 'none',
    'transitionOut'		: 'none'
    });
});