<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 6/3/12
 * Time: 4:21 PM
 * To change this template use File | Settings | File Templates.
 */
require_once("framework/lib/setup.php");
$news  = News::find_by_sql("
CREATE TABLE `certificate` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `headline_sr` varchar(128),
  `headline_en` varchar(128),
  `headline_ru` varchar(128),
  `subline_sr` text,
  `subline_en` text,
  `subline_ru` text,
  `active` int(1),
  `file` varchar(128),
  `url` varchar(256),
  PRIMARY KEY (`id`)
)");
//$news = News::find_by_sql("desc news");
//print_r($news);